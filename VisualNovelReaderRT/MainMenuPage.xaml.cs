﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;


namespace VisualNovelReaderRT
{
    /// <summary>
    /// The root page used to display the game.
    /// </summary>
    public sealed partial class MainMenuPage : Page
    {
        public MainMenuPage()
        {
            InitializeComponent();             
        }

        private void NewGameButton_Click(object sender, RoutedEventArgs e)
        {
            Frame currentFrame = Window.Current.Content as Frame;
            currentFrame.Navigate(typeof(NewGamePage));
        }

        private void LoadGameButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void OptionsButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
