﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.Static.BitmapHandler
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;

namespace VisualNovelEditorWPF.Static
{
  public static class BitmapHandler
  {
    public static BitmapImage getBitmapImage(string tempPath)
    {
      BitmapImage bitmapImage = new BitmapImage();
      bitmapImage.BeginInit();
      try
      {
        FileStream fileStream = new FileStream(tempPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        MemoryStream memoryStream = new MemoryStream(BitmapHandler.ReadStreamFully((Stream) fileStream));
        fileStream.Close();
        bitmapImage.StreamSource = (Stream) memoryStream;
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Error retrieving a Resource, please recheck your Directorys. \n If this error appears again try deleting your Templates Directory");
        return (BitmapImage) null;
      }
      bitmapImage.EndInit();
      return bitmapImage;
    }

    public static byte[] ReadStreamFully(Stream tempFileStreamInput)
    {
      using (MemoryStream memoryStream = new MemoryStream())
      {
        tempFileStreamInput.CopyTo((Stream) memoryStream);
        return memoryStream.ToArray();
      }
    }
  }
}
