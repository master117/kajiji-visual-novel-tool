﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.Static.VisualNovelTemplateHandler
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using VisualNovelEditorWPF.CustomDataTypes;
using VisualNovelEditorWPF.Properties;

namespace VisualNovelEditorWPF.Static
{
  public static class VisualNovelTemplateHandler
  {
    public static bool CheckForExistingTemplate(string templateName)
    {
      return File.Exists(Settings.Default.TemplateDirectory + templateName + ".vnt");
    }

    public static BitmapImage GetBitmapImage(string tempPath)
    {
      return BitmapHandler.getBitmapImage(tempPath);
    }

    internal static Scene GetTemplateFromFile(string tempFileName)
    {
      try
      {
        return Scene.LoadFromFile(tempFileName);
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show(tempFileName + " is no valid template, please fix the file or delete it");
        return (Scene) null;
      }
    }

    public static void TemplateCreateDirectorys(string templateName)
    {
      Directory.CreateDirectory(Settings.Default.TemplateDirectory);
      Directory.CreateDirectory(Settings.Default.TemplateDirectory + templateName + Settings.Default.ResourcesDirectory);
    }

    public static void CreateNewTemplate(Scene tempTemplate)
    {
      tempTemplate.SaveToFile(Settings.Default.TemplateDirectory + tempTemplate.SceneName + "_temp.vnt");
      File.Delete(Settings.Default.TemplateDirectory + tempTemplate.SceneName + ".vnt");
      File.Copy(Settings.Default.TemplateDirectory + tempTemplate.SceneName + "_temp.vnt", Settings.Default.TemplateDirectory + tempTemplate.SceneName + ".vnt");
      File.Delete(Settings.Default.TemplateDirectory + tempTemplate.SceneName + "_temp.vnt");
    }

    public static void DeleteTemplate(string templateName)
    {
      File.Delete(Settings.Default.TemplateDirectory + templateName + ".vnt");
      Directory.Delete(Settings.Default.TemplateDirectory + templateName + Settings.Default.ResourcesDirectory, true);
    }

    public static void CreateResourceFromCustomFileResource(CustomFileResource tempCustomFileResource, string tempTemplateName)
    {
      try
      {
        File.Copy(tempCustomFileResource.FilePath, Settings.Default.TemplateDirectory + tempTemplateName + Settings.Default.ResourcesDirectory + tempCustomFileResource.FileName);
      }
      catch (PathTooLongException ex)
      {
        int num = (int) MessageBox.Show("The destination template path of the file " + tempCustomFileResource.FileName + " is too long, template creation might not be complete");
      }
    }
  }
}
