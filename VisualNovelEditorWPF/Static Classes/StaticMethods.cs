﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.Static.StaticMethods
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace VisualNovelEditorWPF.Static
{
  public static class StaticMethods
  {
    public static Position StandardSpritePosition;
    public static Position StandardTextBoxPosition;

    public static void UpdateControlPosition(Control tempInnerControl, Grid tempOuterGrid, Position tempPosition)
    {
      double left = tempOuterGrid.ActualWidth / 100.0 * tempPosition.PositionLeft;
      double top = tempOuterGrid.ActualHeight / 100.0 * tempPosition.PositionTop;
      double right = tempOuterGrid.ActualWidth / 100.0 * tempPosition.PositionRight;
      double bottom = tempOuterGrid.ActualHeight / 100.0 * tempPosition.PositionBottom;
      double num1 = tempOuterGrid.ActualWidth - left - right;
      double num2 = tempOuterGrid.ActualHeight - top - bottom;
      tempInnerControl.Margin = new Thickness(left, top, right, bottom);
      tempInnerControl.Width = num1;
      tempInnerControl.Height = num2;
    }

    internal static void UpdateControlPosition(Border tempInnerControl, Grid tempOuterGrid, Position tempPosition)
    {
      double left = tempOuterGrid.ActualWidth / 100.0 * tempPosition.PositionLeft;
      double top = tempOuterGrid.ActualHeight / 100.0 * tempPosition.PositionTop;
      double right = tempOuterGrid.ActualWidth / 100.0 * tempPosition.PositionRight;
      double bottom = tempOuterGrid.ActualHeight / 100.0 * tempPosition.PositionBottom;
      double num1 = tempOuterGrid.ActualWidth - left - right;
      double num2 = tempOuterGrid.ActualHeight - top - bottom;
      tempInnerControl.Margin = new Thickness(left, top, right, bottom);
      tempInnerControl.Width = num1;
      tempInnerControl.Height = num2;
    }

    public static void UpdateControlPosition(Image tempInnerControl, Grid tempOuterGrid, Position tempPosition)
    {
      double left = tempOuterGrid.ActualWidth / 100.0 * tempPosition.PositionLeft;
      double top = tempOuterGrid.ActualHeight / 100.0 * tempPosition.PositionTop;
      double right = tempOuterGrid.ActualWidth / 100.0 * tempPosition.PositionRight;
      double bottom = tempOuterGrid.ActualHeight / 100.0 * tempPosition.PositionBottom;
      double num1 = tempOuterGrid.ActualWidth - left - right;
      double num2 = tempOuterGrid.ActualHeight - top - bottom;
      tempInnerControl.Margin = new Thickness(left, top, right, bottom);
      tempInnerControl.Width = num1;
      tempInnerControl.Height = num2;
    }

    public static void UpdateControlPositionFixedSizeGrid(Image tempInnerControl, Grid tempOuterGrid, Position tempPosition)
    {
      double left = tempOuterGrid.Width / 100.0 * tempPosition.PositionLeft;
      double top = tempOuterGrid.Height / 100.0 * tempPosition.PositionTop;
      double right = tempOuterGrid.Width / 100.0 * tempPosition.PositionRight;
      double bottom = tempOuterGrid.Height / 100.0 * tempPosition.PositionBottom;
      double num1 = tempOuterGrid.Width - left - right;
      double num2 = tempOuterGrid.Height - top - bottom;
      tempInnerControl.Margin = new Thickness(left, top, right, bottom);
      tempInnerControl.Width = num1;
      tempInnerControl.Height = num2;
    }

    internal static Position GetControlTPosition(Border tempInnerControl, Grid tempOuterGrid)
    {
      Position position = new Position();
      Point screen1 = tempInnerControl.PointToScreen(new Point(0.0, 0.0));
      Point screen2 = tempOuterGrid.PointToScreen(new Point(0.0, 0.0));
      double num1 = screen1.X - screen2.X;
      double num2 = screen1.Y - screen2.Y;
      position.PositionLeft = num1 * 100.0 / tempOuterGrid.ActualWidth;
      position.PositionTop = num2 * 100.0 / tempOuterGrid.ActualHeight;
      double num3 = screen2.X + tempOuterGrid.ActualWidth - (screen1.X + tempInnerControl.ActualWidth);
      double num4 = screen2.Y + tempOuterGrid.ActualHeight - (screen1.Y + tempInnerControl.ActualHeight);
      position.PositionRight = num3 * 100.0 / tempOuterGrid.ActualWidth;
      position.PositionBottom = num4 * 100.0 / tempOuterGrid.ActualHeight;
      return position;
    }

    internal static Position GetControlTPositionSafe(Border tempInnerControl, Grid tempOuterGrid)
    {
      Position position = new Position();
      Point screen1 = tempInnerControl.PointToScreen(new Point(0.0, 0.0));
      Point screen2 = tempOuterGrid.PointToScreen(new Point(0.0, 0.0));
      double num1 = screen1.X - screen2.X;
      double num2 = screen1.Y - screen2.Y;
      if (num1 < 0.0)
      {
        num1 = 0.0;
        position.PositionLeft = 0.0;
      }
      else
        position.PositionLeft = num1 * 100.0 / tempOuterGrid.ActualWidth;
      if (num2 < 0.0)
      {
        num2 = 0.0;
        position.PositionTop = 0.0;
      }
      else
        position.PositionTop = num2 * 100.0 / tempOuterGrid.ActualHeight;
      double num3 = tempOuterGrid.ActualWidth - (num1 + tempInnerControl.ActualWidth);
      double num4 = tempOuterGrid.ActualHeight - (num2 + tempInnerControl.ActualHeight);
      if (num3 < 0.0)
      {
        position.PositionRight = 0.0;
        position.PositionLeft = 100.0 - tempInnerControl.ActualWidth * 100.0 / tempOuterGrid.ActualWidth;
      }
      else
        position.PositionRight = num3 * 100.0 / tempOuterGrid.ActualWidth;
      if (num4 < 0.0)
      {
        position.PositionBottom = 0.0;
        position.PositionTop = 100.0 - tempInnerControl.ActualHeight * 100.0 / tempOuterGrid.ActualHeight;
      }
      else
        position.PositionBottom = num4 * 100.0 / tempOuterGrid.ActualHeight;
      return position;
    }

    public static Image FlipImageVertical(Image tempImage)
    {
      tempImage.RenderTransformOrigin = new Point(0.5, 0.5);
      tempImage.RenderTransform = (Transform) new ScaleTransform()
      {
        ScaleX = -1.0
      };
      return tempImage;
    }

    public static Image FlipImageHorizontal(Image tempImage)
    {
      tempImage.RenderTransformOrigin = new Point(0.5, 0.5);
      tempImage.RenderTransform = (Transform) new ScaleTransform()
      {
        ScaleY = -1.0
      };
      return tempImage;
    }
  }
}
