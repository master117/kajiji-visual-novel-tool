﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.Static.SoundPlayerHandler
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using NAudio.Wave;
using NAudio.WindowsMediaFormat;
using System;
using System.Windows;
using System.Windows.Threading;

namespace VisualNovelEditorWPF.Static
{
  public static class SoundPlayerHandler
  {
    private static readonly IWavePlayer waveOutDevice = (IWavePlayer) new WaveOut();
    private static readonly DispatcherTimer loopTimer = new DispatcherTimer();
    private static WaveStream mainOutputStream;
    private static WaveChannel32 volumeStream;
    private static string soundFilePath;

    static SoundPlayerHandler()
    {
      SoundPlayerHandler.loopTimer.Tick += new EventHandler(SoundPlayerHandler.waveOutDevice_PlaybackStopped);
    }

    public static void TurnOn()
    {
      SoundPlayerHandler.waveOutDevice.Play();
      SoundPlayerHandler.loopTimer.Interval = SoundPlayerHandler.mainOutputStream.TotalTime;
      SoundPlayerHandler.loopTimer.Start();
    }

    public static void TurnOff()
    {
      SoundPlayerHandler.loopTimer.Stop();
      if (!SoundPlayerHandler.CheckStatus())
        return;
      SoundPlayerHandler.waveOutDevice.Stop();
    }

    public static void ChangeSong(string tempPath)
    {
      try
      {
        if (tempPath != null)
        {
          SoundPlayerHandler.mainOutputStream = SoundPlayerHandler.CreateInputStream(tempPath);
          SoundPlayerHandler.soundFilePath = tempPath;
        }
        if (SoundPlayerHandler.CheckStatus())
        {
          SoundPlayerHandler.waveOutDevice.Init((IWaveProvider) SoundPlayerHandler.mainOutputStream);
          SoundPlayerHandler.TurnOn();
        }
        else
          SoundPlayerHandler.waveOutDevice.Init((IWaveProvider) SoundPlayerHandler.mainOutputStream);
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Invalid or Missing Soundfile please check: " + tempPath);
      }
    }

    public static bool CheckStatus()
    {
      return SoundPlayerHandler.waveOutDevice != null && SoundPlayerHandler.waveOutDevice.PlaybackState != PlaybackState.Stopped;
    }

    public static void Repeat()
    {
      if (SoundPlayerHandler.soundFilePath == null)
        return;
      SoundPlayerHandler.mainOutputStream = SoundPlayerHandler.CreateInputStream(SoundPlayerHandler.soundFilePath);
      SoundPlayerHandler.waveOutDevice.Init((IWaveProvider) SoundPlayerHandler.mainOutputStream);
      SoundPlayerHandler.TurnOn();
    }

    private static void waveOutDevice_PlaybackStopped(object sender, EventArgs e)
    {
      SoundPlayerHandler.loopTimer.Stop();
      SoundPlayerHandler.Repeat();
    }

    private static WaveStream CreateInputStream(string fileName)
    {
      WaveChannel32 waveChannel32 = (WaveChannel32) null;
      if (fileName.EndsWith(".mp3"))
        waveChannel32 = new WaveChannel32((WaveStream) new Mp3FileReader(fileName));
      if (fileName.EndsWith(".wav"))
        waveChannel32 = new WaveChannel32((WaveStream) new WaveFileReader(fileName));
      if (fileName.EndsWith(".wma"))
        waveChannel32 = new WaveChannel32((WaveStream) new WMAFileReader(fileName));
      if (fileName.EndsWith(".aiff"))
        waveChannel32 = new WaveChannel32((WaveStream) new AiffFileReader(fileName));
      if (waveChannel32 == null)
        throw new InvalidOperationException("Unsupported extension");
      SoundPlayerHandler.volumeStream = waveChannel32;
      return (WaveStream) SoundPlayerHandler.volumeStream;
    }
  }
}
