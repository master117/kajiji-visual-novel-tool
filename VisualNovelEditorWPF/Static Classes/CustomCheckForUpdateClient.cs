﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.Static.CustomCheckForUpdateClient
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.Net.Sockets;
using System.Text;
using System.Windows;

namespace VisualNovelEditorWPF.Static
{
  public static class CustomCheckForUpdateClient
  {
    public static bool CheckForUpdate(string versionNumber, string address, int port)
    {
      try
      {
        Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        byte[] bytes = Encoding.ASCII.GetBytes(versionNumber);
        socket.Connect(address, port);
        socket.Send(bytes);
        byte[] numArray = new byte[1024];
        int count = socket.Receive(numArray);
        return Encoding.ASCII.GetString(numArray, 0, count) == "OK";
      }
      catch (SocketException ex)
      {
        Console.WriteLine(ex.ToString());
        int num = (int) MessageBox.Show("Could not connect to Update Server");
        return false;
      }
    }
  }
}
