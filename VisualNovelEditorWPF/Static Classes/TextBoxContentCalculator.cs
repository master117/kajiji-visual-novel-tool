﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.Static.TextBoxContentCalculator
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace VisualNovelEditorWPF.Static
{
  internal class TextBoxContentCalculator
  {
    public static string[] CalculateContent(string tempContent, string tempSpeakername, TextBox tempTextbox)
    {
      char[] chArray = new char[2]{ ' ', '\n' };
      string[] strArray = tempContent.Split(chArray);
      string str = "";
      double height = new FormattedText("A", CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface(tempTextbox.FontFamily.ToString()), tempTextbox.FontSize, (Brush) Brushes.Black).Height;
      List<string> stringList = new List<string>();
      if (strArray.Length == 1 && strArray[0] == "")
      {
        stringList.Add("");
        return stringList.ToArray();
      }
      int index = 0;
      if (Math.Abs(tempTextbox.ViewportHeight) == 0.0)
        return (string[]) null;
      while (index < strArray.Length)
      {
        double num1 = height;
        while (num1 <= tempTextbox.ViewportHeight && index < strArray.Length)
        {
          double num2 = 0.0;
          for (; index < strArray.Length; ++index)
          {
            FormattedText formattedText = new FormattedText(strArray[index] + ".", CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface(tempTextbox.FontFamily.ToString()), tempTextbox.FontSize, (Brush) Brushes.Black);
            if (num2 + formattedText.Width < tempTextbox.ViewportWidth)
            {
              str = str + strArray[index] + " ";
              num2 += formattedText.Width;
            }
            else
              break;
          }
          str += "\n";
          num1 += height;
        }
        stringList.Add(str);
        str = "";
      }
      return stringList.ToArray();
    }
  }
}
