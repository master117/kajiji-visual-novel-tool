﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.Static.VisualNovelHandler
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System.Collections.Generic;
using System.IO;
using System.Security.AccessControl;
using VisualNovelEditorWPF.CustomDataTypes;
using VisualNovelEditorWPF.Properties;

namespace VisualNovelEditorWPF.Static
{
  public static class VisualNovelHandler
  {
    public static VisualNovel mainVisualNovel;

    public static void CreateVisualNovel(string tempNovelName, VisualNovelVisualNovelType tempType)
    {
      VisualNovelHandler.mainVisualNovel = new VisualNovel();
      VisualNovelHandler.mainVisualNovel.VisualNovelName = tempNovelName;
      VisualNovelHandler.mainVisualNovel.VisualNovelType = tempType;
      VisualNovelHandler.CreateDirectorys();
    }

    public static void SetVisualNovelFromPath(string tempPath)
    {
      VisualNovelHandler.mainVisualNovel = VisualNovel.LoadFromFile(tempPath);
      VisualNovelHandler.CreateDirectorys();
    }

    public static List<Scene> GetScenes()
    {
      if (VisualNovelHandler.mainVisualNovel != null)
        return VisualNovelHandler.mainVisualNovel.Scene;
      return (List<Scene>) null;
    }

    public static Scene GetScene(string sceneName)
    {
      foreach (Scene tScene in VisualNovelHandler.mainVisualNovel.Scene)
      {
        if (tScene.SceneName == sceneName)
          return tScene;
      }
      return (Scene) null;
    }

    public static bool CheckForExistingNovel(string novelName)
    {
      return File.Exists(Settings.Default.VisualNovelDirectory + novelName + ".vn");
    }

    public static List<Attribute> GetAttributes()
    {
      return VisualNovelHandler.mainVisualNovel.Attribute;
    }

    public static VisualNovelVisualNovelType GetNovelType()
    {
      return VisualNovelHandler.mainVisualNovel.VisualNovelType;
    }

    public static bool CheckForEmptyStartScene()
    {
      return VisualNovelHandler.mainVisualNovel.StartScene == null;
    }

    public static List<Button> GetTButton(Scene tempStartScene, Button tempCompareButton)
    {
      List<Button> tButtonList = new List<Button>();
      foreach (Button tButton in tempStartScene.Button)
      {
        if (tButton.NextSceneName == tempCompareButton.NextSceneName && (tempCompareButton.ButtonText == null || tButton.ButtonText == tempCompareButton.ButtonText))
          tButtonList.Add(tButton);
      }
      return tButtonList;
    }

    public static void AddStartScene(string tempStartScene)
    {
      VisualNovelHandler.mainVisualNovel.StartScene = tempStartScene;
    }

    public static void AddAttribute(Attribute tempAttribute)
    {
      VisualNovelHandler.mainVisualNovel.Attribute.Add(tempAttribute);
    }

    public static void AddScene(Scene tempScene)
    {
      VisualNovelHandler.mainVisualNovel.Scene.Add(tempScene);
    }

    public static bool AddButton(Button tempButton, Scene tempScene)
    {
      if (tempScene.Button.Contains(tempButton))
        return false;
      tempScene.Button.Add(tempButton);
      return true;
    }

    public static void DeleteScene(string sceneName)
    {
      Scene scene = VisualNovelHandler.GetScene(sceneName);
      if (scene == null)
        return;
      VisualNovelHandler.mainVisualNovel.Scene.Remove(scene);
    }

    public static void DeleteScene(Scene tempScene)
    {
      VisualNovelHandler.mainVisualNovel.Scene.Remove(tempScene);
    }

    public static void DeleteTButton(Button tempButton)
    {
      foreach (Scene tScene in VisualNovelHandler.mainVisualNovel.Scene)
      {
        if (tScene.Button.Contains(tempButton))
        {
          tScene.Button.Remove(tempButton);
          break;
        }
      }
    }

    public static void DeleteTButton(Button tempButton, Scene tempScene)
    {
      tempScene.Button.Remove(tempButton);
    }

    private static void CreateDirectorys()
    {
      Directory.CreateDirectory(Settings.Default.VisualNovelDirectory);
      Directory.CreateDirectory(Settings.Default.VisualNovelDirectory + VisualNovelHandler.mainVisualNovel.VisualNovelName + Settings.Default.ResourcesDirectory);
      Directory.SetAccessControl(Settings.Default.VisualNovelDirectory, new DirectorySecurity());
    }

    public static void SaveVisualNovel()
    {
      try
      {
        // ISSUE: variable of a compiler-generated type
        Settings settings = Settings.Default;
        VisualNovelHandler.mainVisualNovel.SaveToFile(settings.VisualNovelDirectory + VisualNovelHandler.mainVisualNovel.VisualNovelName + settings.VNTempFileDescriptor);
        File.Delete(settings.VisualNovelDirectory + VisualNovelHandler.mainVisualNovel.VisualNovelName + settings.VNFileDescriptor);
        File.Copy(settings.VisualNovelDirectory + VisualNovelHandler.mainVisualNovel.VisualNovelName + settings.VNTempFileDescriptor, settings.VisualNovelDirectory + VisualNovelHandler.mainVisualNovel.VisualNovelName + settings.VNFileDescriptor);
        File.Delete(settings.VisualNovelDirectory + VisualNovelHandler.mainVisualNovel.VisualNovelName + settings.VNTempFileDescriptor);
      }
      catch (DirectoryNotFoundException ex)
      {
        VisualNovelHandler.CreateDirectorys();
        VisualNovelHandler.SaveVisualNovel();
      }
    }

    public static void CreateResource(CustomFileResource tempResource)
    {
      if (tempResource == null)
        return;
      try
      {
        if (!File.Exists(Settings.Default.VisualNovelDirectory + VisualNovelHandler.mainVisualNovel.VisualNovelName + Settings.Default.ResourcesDirectory + tempResource.FileName))
          File.Copy(tempResource.FilePath, Settings.Default.VisualNovelDirectory + VisualNovelHandler.mainVisualNovel.VisualNovelName + Settings.Default.ResourcesDirectory + tempResource.FileName);
      }
      catch (DirectoryNotFoundException ex)
      {
        VisualNovelHandler.CreateDirectorys();
        VisualNovelHandler.CreateResource(tempResource);
      }
    }
  }
}
