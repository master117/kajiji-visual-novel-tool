﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.SplashScreenWindow
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using VisualNovelEditorWPF.Properties;
using VisualNovelEditorWPF.Static;

namespace VisualNovelEditorWPF
{
  public partial class SplashScreenWindow : Window, IComponentConnector
  {
    public static event SplashScreenWindow.ContentLoadedDelegate ContentLoaded;

    public SplashScreenWindow()
    {
      this.InitializeComponent();
    }

    protected override void OnContentRendered(EventArgs e)
    {
      base.OnContentRendered(e);
      // ISSUE: variable of a compiler-generated type
      Settings settings = Settings.Default;
            /*
      if (DateTime.Now > DateTime.Parse("10/10/2013"))
      {
        int num = (int) MessageBox.Show("This Version of the Programm has expired, please check for an Update");
        Application.Current.Shutdown(0);
      }
      */
      this.LoadStandardContent();
      Thread.Sleep(2000);
      SplashScreenWindow.ContentLoaded();
      this.Close();
    }

    public void LoadStandardContent()
    {
      StaticMethods.StandardSpritePosition = new Position();
      StaticMethods.StandardSpritePosition.PositionLeft = 30.0;
      StaticMethods.StandardSpritePosition.PositionTop = 5.0;
      StaticMethods.StandardSpritePosition.PositionRight = 30.0;
      StaticMethods.StandardSpritePosition.PositionBottom = 0.0;
      StaticMethods.StandardTextBoxPosition = new Position();
      StaticMethods.StandardTextBoxPosition.PositionLeft = 2.0;
      StaticMethods.StandardTextBoxPosition.PositionTop = 60.0;
      StaticMethods.StandardTextBoxPosition.PositionRight = 2.0;
      StaticMethods.StandardTextBoxPosition.PositionBottom = 2.0;
    }

    public delegate void ContentLoadedDelegate();
  }
}
