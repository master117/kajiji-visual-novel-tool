﻿using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Windows;

namespace VisualNovelEditorWPF
{
  public partial class App : Application
  {
    public App()
    {
      SplashScreenWindow.ContentLoaded += new SplashScreenWindow.ContentLoadedDelegate(this.OpenStartWindow);
    }

    private Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
    {
      string name = (args.Name.Contains<char>(',') ? args.Name.Substring(0, args.Name.IndexOf(',')) : args.Name.Replace(".dll", "")).Replace(".", "_");
      if (name.EndsWith("_resources"))
        return (Assembly) null;
      return Assembly.Load((byte[]) new ResourceManager(this.GetType().Namespace + "Properties.Resources", Assembly.GetExecutingAssembly()).GetObject(name));
    }

    public void OpenStartWindow()
    {
      MainWindow mainWindow = new MainWindow();
      Application.Current.MainWindow = (Window) mainWindow;
      mainWindow.Show();
    }
  }
}
