﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.SceneMusic
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using VisualNovelEditorWPF.CustomDataTypes;

namespace VisualNovelEditorWPF
{
  public class SceneMusic
  {
    private string musicFilePathField;
    private MusicRepeat musicRepeatField;
    private bool musicRepeatFieldSpecified;
    private MusicStartType musicStartTypeField;
    private bool musicStartTypeFieldSpecified;
    private int musicStartTimeField;
    private bool musicStartTimeFieldSpecified;
    private int musicDurationField;
    private bool musicDurationFieldSpecified;
    private static XmlSerializer serializer;

    public string MusicFilePath
    {
      get
      {
        return this.musicFilePathField;
      }
      set
      {
        this.musicFilePathField = value;
      }
    }

    public MusicRepeat MusicRepeat
    {
      get
      {
        return this.musicRepeatField;
      }
      set
      {
        this.musicRepeatField = value;
      }
    }

    [XmlIgnore]
    public bool MusicRepeatSpecified
    {
      get
      {
        return this.musicRepeatFieldSpecified;
      }
      set
      {
        this.musicRepeatFieldSpecified = value;
      }
    }

    public MusicStartType MusicStartType
    {
      get
      {
        return this.musicStartTypeField;
      }
      set
      {
        this.musicStartTypeField = value;
      }
    }

    [XmlIgnore]
    public bool MusicStartTypeSpecified
    {
      get
      {
        return this.musicStartTypeFieldSpecified;
      }
      set
      {
        this.musicStartTypeFieldSpecified = value;
      }
    }

    public int MusicStartTime
    {
      get
      {
        return this.musicStartTimeField;
      }
      set
      {
        this.musicStartTimeField = value;
      }
    }

    [XmlIgnore]
    public bool MusicStartTimeSpecified
    {
      get
      {
        return this.musicStartTimeFieldSpecified;
      }
      set
      {
        this.musicStartTimeFieldSpecified = value;
      }
    }

    public int MusicDuration
    {
      get
      {
        return this.musicDurationField;
      }
      set
      {
        this.musicDurationField = value;
      }
    }

    [XmlIgnore]
    public bool MusicDurationSpecified
    {
      get
      {
        return this.musicDurationFieldSpecified;
      }
      set
      {
        this.musicDurationFieldSpecified = value;
      }
    }

    private static XmlSerializer Serializer
    {
      get
      {
        if (SceneMusic.serializer == null)
          SceneMusic.serializer = new XmlSerializer(typeof (SceneMusic));
        return SceneMusic.serializer;
      }
    }

    public virtual string Serialize()
    {
      StreamReader streamReader = (StreamReader) null;
      MemoryStream memoryStream = (MemoryStream) null;
      try
      {
        memoryStream = new MemoryStream();
        SceneMusic.Serializer.Serialize((Stream) memoryStream, (object) this);
        memoryStream.Seek(0L, SeekOrigin.Begin);
        streamReader = new StreamReader((Stream) memoryStream);
        return streamReader.ReadToEnd();
      }
      finally
      {
        if (streamReader != null)
          streamReader.Dispose();
        if (memoryStream != null)
          memoryStream.Dispose();
      }
    }

    public static bool Deserialize(string xml, out SceneMusic obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (SceneMusic) null;
      try
      {
        obj = SceneMusic.Deserialize(xml);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool Deserialize(string xml, out SceneMusic obj)
    {
      Exception exception = (Exception) null;
      return SceneMusic.Deserialize(xml, out obj, out exception);
    }

    public static SceneMusic Deserialize(string xml)
    {
      StringReader stringReader = (StringReader) null;
      try
      {
        stringReader = new StringReader(xml);
        return (SceneMusic) SceneMusic.Serializer.Deserialize(XmlReader.Create((TextReader) stringReader));
      }
      finally
      {
        if (stringReader != null)
          stringReader.Dispose();
      }
    }

    public virtual bool SaveToFile(string fileName, out Exception exception)
    {
      exception = (Exception) null;
      try
      {
        this.SaveToFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public virtual void SaveToFile(string fileName)
    {
      StreamWriter streamWriter = (StreamWriter) null;
      try
      {
        string str = this.Serialize();
        streamWriter = new FileInfo(fileName).CreateText();
        streamWriter.WriteLine(str);
        streamWriter.Close();
      }
      finally
      {
        if (streamWriter != null)
          streamWriter.Dispose();
      }
    }

    public static bool LoadFromFile(string fileName, out SceneMusic obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (SceneMusic) null;
      try
      {
        obj = SceneMusic.LoadFromFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool LoadFromFile(string fileName, out SceneMusic obj)
    {
      Exception exception = (Exception) null;
      return SceneMusic.LoadFromFile(fileName, out obj, out exception);
    }

    public static SceneMusic LoadFromFile(string fileName)
    {
      FileStream fileStream = (FileStream) null;
      StreamReader streamReader = (StreamReader) null;
      try
      {
        fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
        streamReader = new StreamReader((Stream) fileStream);
        string end = streamReader.ReadToEnd();
        streamReader.Close();
        fileStream.Close();
        return SceneMusic.Deserialize(end);
      }
      finally
      {
        if (fileStream != null)
          fileStream.Dispose();
        if (streamReader != null)
          streamReader.Dispose();
      }
    }
  }
}
