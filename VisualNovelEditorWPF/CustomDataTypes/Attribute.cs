﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.Attribute
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace VisualNovelEditorWPF
{
  public class Attribute
  {
    private string attributeNameField;
    private string startValueField;
    private string endValueField;
    private static XmlSerializer serializer;

    public string AttributeName
    {
      get
      {
        return this.attributeNameField;
      }
      set
      {
        this.attributeNameField = value;
      }
    }

    public string StartValue
    {
      get
      {
        return this.startValueField;
      }
      set
      {
        this.startValueField = value;
      }
    }

    public string EndValue
    {
      get
      {
        return this.endValueField;
      }
      set
      {
        this.endValueField = value;
      }
    }

    private static XmlSerializer Serializer
    {
      get
      {
        if (Attribute.serializer == null)
          Attribute.serializer = new XmlSerializer(typeof (Attribute));
        return Attribute.serializer;
      }
    }

    public virtual string Serialize()
    {
      StreamReader streamReader = (StreamReader) null;
      MemoryStream memoryStream = (MemoryStream) null;
      try
      {
        memoryStream = new MemoryStream();
        Attribute.Serializer.Serialize((Stream) memoryStream, (object) this);
        memoryStream.Seek(0L, SeekOrigin.Begin);
        streamReader = new StreamReader((Stream) memoryStream);
        return streamReader.ReadToEnd();
      }
      finally
      {
        if (streamReader != null)
          streamReader.Dispose();
        if (memoryStream != null)
          memoryStream.Dispose();
      }
    }

    public static bool Deserialize(string xml, out Attribute obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (Attribute) null;
      try
      {
        obj = Attribute.Deserialize(xml);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool Deserialize(string xml, out Attribute obj)
    {
      Exception exception = (Exception) null;
      return Attribute.Deserialize(xml, out obj, out exception);
    }

    public static Attribute Deserialize(string xml)
    {
      StringReader stringReader = (StringReader) null;
      try
      {
        stringReader = new StringReader(xml);
        return (Attribute) Attribute.Serializer.Deserialize(XmlReader.Create((TextReader) stringReader));
      }
      finally
      {
        if (stringReader != null)
          stringReader.Dispose();
      }
    }

    public virtual bool SaveToFile(string fileName, out Exception exception)
    {
      exception = (Exception) null;
      try
      {
        this.SaveToFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public virtual void SaveToFile(string fileName)
    {
      StreamWriter streamWriter = (StreamWriter) null;
      try
      {
        string str = this.Serialize();
        streamWriter = new FileInfo(fileName).CreateText();
        streamWriter.WriteLine(str);
        streamWriter.Close();
      }
      finally
      {
        if (streamWriter != null)
          streamWriter.Dispose();
      }
    }

    public static bool LoadFromFile(string fileName, out Attribute obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (Attribute) null;
      try
      {
        obj = Attribute.LoadFromFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool LoadFromFile(string fileName, out Attribute obj)
    {
      Exception exception = (Exception) null;
      return Attribute.LoadFromFile(fileName, out obj, out exception);
    }

    public static Attribute LoadFromFile(string fileName)
    {
      FileStream fileStream = (FileStream) null;
      StreamReader streamReader = (StreamReader) null;
      try
      {
        fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
        streamReader = new StreamReader((Stream) fileStream);
        string end = streamReader.ReadToEnd();
        streamReader.Close();
        fileStream.Close();
        return Attribute.Deserialize(end);
      }
      finally
      {
        if (fileStream != null)
          fileStream.Dispose();
        if (streamReader != null)
          streamReader.Dispose();
      }
    }
  }
}
