﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.CustomDataTypes.LoopStream
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using NAudio.Wave;

namespace VisualNovelEditorWPF.CustomDataTypes
{
  public class LoopStream : WaveStream
  {
    private readonly WaveStream sourceStream;

    public bool EnableLooping { get; set; }

    public override WaveFormat WaveFormat
    {
      get
      {
        return this.sourceStream.WaveFormat;
      }
    }

    public override long Length
    {
      get
      {
        return this.sourceStream.Length;
      }
    }

    public override long Position
    {
      get
      {
        return this.sourceStream.Position;
      }
      set
      {
        this.sourceStream.Position = value;
      }
    }

    public LoopStream(WaveStream sourceStream)
    {
      this.sourceStream = sourceStream;
      this.EnableLooping = true;
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
      int num1 = 0;
      while (num1 < count)
      {
        int num2 = this.sourceStream.Read(buffer, offset + num1, count - num1);
        if (num2 == 0)
        {
          if (this.sourceStream.Position != 0L && this.EnableLooping)
            this.sourceStream.Position = 0L;
          else
            break;
        }
        num1 += num2;
      }
      return num1;
    }
  }
}
