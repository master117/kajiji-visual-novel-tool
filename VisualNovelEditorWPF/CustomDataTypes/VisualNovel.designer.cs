﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.VisualNovel
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace VisualNovelEditorWPF
{
  public class VisualNovel
  {
    private string visualNovelNameField;
    private string startSceneField;
    private VisualNovelVisualNovelType visualNovelTypeField;
    private bool visualNovelTypeFieldSpecified;
    private List<Attribute> attributeField;
    private List<Scene> sceneField;
    private static XmlSerializer serializer;

    public string VisualNovelName
    {
      get
      {
        return this.visualNovelNameField;
      }
      set
      {
        this.visualNovelNameField = value;
      }
    }

    public string StartScene
    {
      get
      {
        return this.startSceneField;
      }
      set
      {
        this.startSceneField = value;
      }
    }

    public VisualNovelVisualNovelType VisualNovelType
    {
      get
      {
        return this.visualNovelTypeField;
      }
      set
      {
        this.visualNovelTypeField = value;
      }
    }

    [XmlIgnore]
    public bool VisualNovelTypeSpecified
    {
      get
      {
        return this.visualNovelTypeFieldSpecified;
      }
      set
      {
        this.visualNovelTypeFieldSpecified = value;
      }
    }

    public List<Attribute> Attribute
    {
      get
      {
        return this.attributeField;
      }
      set
      {
        this.attributeField = value;
      }
    }

    public List<Scene> Scene
    {
      get
      {
        return this.sceneField;
      }
      set
      {
        this.sceneField = value;
      }
    }

    private static XmlSerializer Serializer
    {
      get
      {
        if (VisualNovel.serializer == null)
          VisualNovel.serializer = new XmlSerializer(typeof (VisualNovel));
        return VisualNovel.serializer;
      }
    }

    public VisualNovel()
    {
      this.sceneField = new List<Scene>();
      this.attributeField = new List<Attribute>();
    }

    public virtual string Serialize()
    {
      StreamReader streamReader = (StreamReader) null;
      MemoryStream memoryStream = (MemoryStream) null;
      try
      {
        memoryStream = new MemoryStream();
        VisualNovel.Serializer.Serialize((Stream) memoryStream, (object) this);
        memoryStream.Seek(0L, SeekOrigin.Begin);
        streamReader = new StreamReader((Stream) memoryStream);
        return streamReader.ReadToEnd();
      }
      finally
      {
        if (streamReader != null)
          streamReader.Dispose();
        if (memoryStream != null)
          memoryStream.Dispose();
      }
    }

    public static bool Deserialize(string xml, out VisualNovel obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (VisualNovel) null;
      try
      {
        obj = VisualNovel.Deserialize(xml);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool Deserialize(string xml, out VisualNovel obj)
    {
      Exception exception = (Exception) null;
      return VisualNovel.Deserialize(xml, out obj, out exception);
    }

    public static VisualNovel Deserialize(string xml)
    {
      StringReader stringReader = (StringReader) null;
      try
      {
        stringReader = new StringReader(xml);
        return (VisualNovel) VisualNovel.Serializer.Deserialize(XmlReader.Create((TextReader) stringReader));
      }
      finally
      {
        if (stringReader != null)
          stringReader.Dispose();
      }
    }

    public virtual bool SaveToFile(string fileName, out Exception exception)
    {
      exception = (Exception) null;
      try
      {
        this.SaveToFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public virtual void SaveToFile(string fileName)
    {
      StreamWriter streamWriter = (StreamWriter) null;
      try
      {
        string str = this.Serialize();
        streamWriter = new FileInfo(fileName).CreateText();
        streamWriter.WriteLine(str);
        streamWriter.Close();
      }
      finally
      {
        if (streamWriter != null)
          streamWriter.Dispose();
      }
    }

    public static bool LoadFromFile(string fileName, out VisualNovel obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (VisualNovel) null;
      try
      {
        obj = VisualNovel.LoadFromFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool LoadFromFile(string fileName, out VisualNovel obj)
    {
      Exception exception = (Exception) null;
      return VisualNovel.LoadFromFile(fileName, out obj, out exception);
    }

    public static VisualNovel LoadFromFile(string fileName)
    {
      FileStream fileStream = (FileStream) null;
      StreamReader streamReader = (StreamReader) null;
      try
      {
        fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
        streamReader = new StreamReader((Stream) fileStream);
        string end = streamReader.ReadToEnd();
        streamReader.Close();
        fileStream.Close();
        return VisualNovel.Deserialize(end);
      }
      finally
      {
        if (fileStream != null)
          fileStream.Dispose();
        if (streamReader != null)
          streamReader.Dispose();
      }
    }
  }
}
