﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.CustomDataTypes.CustomEdge`1
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using QuickGraph;

namespace VisualNovelEditorWPF.CustomDataTypes
{
  internal class CustomEdge<CustomVertex> : Edge<CustomVertex>
  {
    public string mainText { get; private set; }

    public CustomEdge(string text, CustomVertex source, CustomVertex target)
      : base(source, target)
    {
      this.mainText = text;
    }

    public override string ToString()
    {
      return this.mainText;
    }
  }
}
