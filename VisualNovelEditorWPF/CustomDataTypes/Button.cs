﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.Button
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace VisualNovelEditorWPF
{
  public class Button
  {
    private string buttonTextField;
    private string nextSceneNameField;
    private List<ButtonAttribute> buttonAttributeField;
    private static XmlSerializer serializer;

    public string ButtonText
    {
      get
      {
        return this.buttonTextField;
      }
      set
      {
        this.buttonTextField = value;
      }
    }

    public string NextSceneName
    {
      get
      {
        return this.nextSceneNameField;
      }
      set
      {
        this.nextSceneNameField = value;
      }
    }

    public List<ButtonAttribute> ButtonAttribute
    {
      get
      {
        return this.buttonAttributeField;
      }
      set
      {
        this.buttonAttributeField = value;
      }
    }

    private static XmlSerializer Serializer
    {
      get
      {
        if (Button.serializer == null)
          Button.serializer = new XmlSerializer(typeof (Button));
        return Button.serializer;
      }
    }

    public Button()
    {
      this.buttonAttributeField = new List<ButtonAttribute>();
    }

    public virtual string Serialize()
    {
      StreamReader streamReader = (StreamReader) null;
      MemoryStream memoryStream = (MemoryStream) null;
      try
      {
        memoryStream = new MemoryStream();
        Button.Serializer.Serialize((Stream) memoryStream, (object) this);
        memoryStream.Seek(0L, SeekOrigin.Begin);
        streamReader = new StreamReader((Stream) memoryStream);
        return streamReader.ReadToEnd();
      }
      finally
      {
        if (streamReader != null)
          streamReader.Dispose();
        if (memoryStream != null)
          memoryStream.Dispose();
      }
    }

    public static bool Deserialize(string xml, out Button obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (Button) null;
      try
      {
        obj = Button.Deserialize(xml);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool Deserialize(string xml, out Button obj)
    {
      Exception exception = (Exception) null;
      return Button.Deserialize(xml, out obj, out exception);
    }

    public static Button Deserialize(string xml)
    {
      StringReader stringReader = (StringReader) null;
      try
      {
        stringReader = new StringReader(xml);
        return (Button) Button.Serializer.Deserialize(XmlReader.Create((TextReader) stringReader));
      }
      finally
      {
        if (stringReader != null)
          stringReader.Dispose();
      }
    }

    public virtual bool SaveToFile(string fileName, out Exception exception)
    {
      exception = (Exception) null;
      try
      {
        this.SaveToFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public virtual void SaveToFile(string fileName)
    {
      StreamWriter streamWriter = (StreamWriter) null;
      try
      {
        string str = this.Serialize();
        streamWriter = new FileInfo(fileName).CreateText();
        streamWriter.WriteLine(str);
        streamWriter.Close();
      }
      finally
      {
        if (streamWriter != null)
          streamWriter.Dispose();
      }
    }

    public static bool LoadFromFile(string fileName, out Button obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (Button) null;
      try
      {
        obj = Button.LoadFromFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool LoadFromFile(string fileName, out Button obj)
    {
      Exception exception = (Exception) null;
      return Button.LoadFromFile(fileName, out obj, out exception);
    }

    public static Button LoadFromFile(string fileName)
    {
      FileStream fileStream = (FileStream) null;
      StreamReader streamReader = (StreamReader) null;
      try
      {
        fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
        streamReader = new StreamReader((Stream) fileStream);
        string end = streamReader.ReadToEnd();
        streamReader.Close();
        fileStream.Close();
        return Button.Deserialize(end);
      }
      finally
      {
        if (fileStream != null)
          fileStream.Dispose();
        if (streamReader != null)
          streamReader.Dispose();
      }
    }
  }
}
