﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.SceneTextBox
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace VisualNovelEditorWPF
{
  public class SceneTextBox
  {
    private string textBoxImagePathField;
    private Position textBoxPositionField;
    private string speakerField;
    private List<string> textField;
    private string fontField;
    private double fontSizeField;
    private bool fontSizeFieldSpecified;
    private string decorationField;
    private Color colorField;
    private string effectField;
    private string effectStartTypeField;
    private string effectStartTimeField;
    private string effectDurationField;
    private static XmlSerializer serializer;

    public string TextBoxImagePath
    {
      get
      {
        return this.textBoxImagePathField;
      }
      set
      {
        this.textBoxImagePathField = value;
      }
    }

    public Position TextBoxPosition
    {
      get
      {
        return this.textBoxPositionField;
      }
      set
      {
        this.textBoxPositionField = value;
      }
    }

    public string Speaker
    {
      get
      {
        return this.speakerField;
      }
      set
      {
        this.speakerField = value;
      }
    }

    public List<string> Text
    {
      get
      {
        return this.textField;
      }
      set
      {
        this.textField = value;
      }
    }

    public string Font
    {
      get
      {
        return this.fontField;
      }
      set
      {
        this.fontField = value;
      }
    }

    public double FontSize
    {
      get
      {
        return this.fontSizeField;
      }
      set
      {
        this.fontSizeField = value;
      }
    }

    [XmlIgnore]
    public bool FontSizeSpecified
    {
      get
      {
        return this.fontSizeFieldSpecified;
      }
      set
      {
        this.fontSizeFieldSpecified = value;
      }
    }

    public string Decoration
    {
      get
      {
        return this.decorationField;
      }
      set
      {
        this.decorationField = value;
      }
    }

    public Color Color
    {
      get
      {
        return this.colorField;
      }
      set
      {
        this.colorField = value;
      }
    }

    public string Effect
    {
      get
      {
        return this.effectField;
      }
      set
      {
        this.effectField = value;
      }
    }

    public string EffectStartType
    {
      get
      {
        return this.effectStartTypeField;
      }
      set
      {
        this.effectStartTypeField = value;
      }
    }

    public string EffectStartTime
    {
      get
      {
        return this.effectStartTimeField;
      }
      set
      {
        this.effectStartTimeField = value;
      }
    }

    public string EffectDuration
    {
      get
      {
        return this.effectDurationField;
      }
      set
      {
        this.effectDurationField = value;
      }
    }

    private static XmlSerializer Serializer
    {
      get
      {
        if (SceneTextBox.serializer == null)
          SceneTextBox.serializer = new XmlSerializer(typeof (SceneTextBox));
        return SceneTextBox.serializer;
      }
    }

    public SceneTextBox()
    {
      this.colorField = new Color();
      this.textField = new List<string>();
      this.textBoxPositionField = new Position();
    }

    public virtual string Serialize()
    {
      StreamReader streamReader = (StreamReader) null;
      MemoryStream memoryStream = (MemoryStream) null;
      try
      {
        memoryStream = new MemoryStream();
        SceneTextBox.Serializer.Serialize((Stream) memoryStream, (object) this);
        memoryStream.Seek(0L, SeekOrigin.Begin);
        streamReader = new StreamReader((Stream) memoryStream);
        return streamReader.ReadToEnd();
      }
      finally
      {
        if (streamReader != null)
          streamReader.Dispose();
        if (memoryStream != null)
          memoryStream.Dispose();
      }
    }

    public static bool Deserialize(string xml, out SceneTextBox obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (SceneTextBox) null;
      try
      {
        obj = SceneTextBox.Deserialize(xml);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool Deserialize(string xml, out SceneTextBox obj)
    {
      Exception exception = (Exception) null;
      return SceneTextBox.Deserialize(xml, out obj, out exception);
    }

    public static SceneTextBox Deserialize(string xml)
    {
      StringReader stringReader = (StringReader) null;
      try
      {
        stringReader = new StringReader(xml);
        return (SceneTextBox) SceneTextBox.Serializer.Deserialize(XmlReader.Create((TextReader) stringReader));
      }
      finally
      {
        if (stringReader != null)
          stringReader.Dispose();
      }
    }

    public virtual bool SaveToFile(string fileName, out Exception exception)
    {
      exception = (Exception) null;
      try
      {
        this.SaveToFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public virtual void SaveToFile(string fileName)
    {
      StreamWriter streamWriter = (StreamWriter) null;
      try
      {
        string str = this.Serialize();
        streamWriter = new FileInfo(fileName).CreateText();
        streamWriter.WriteLine(str);
        streamWriter.Close();
      }
      finally
      {
        if (streamWriter != null)
          streamWriter.Dispose();
      }
    }

    public static bool LoadFromFile(string fileName, out SceneTextBox obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (SceneTextBox) null;
      try
      {
        obj = SceneTextBox.LoadFromFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool LoadFromFile(string fileName, out SceneTextBox obj)
    {
      Exception exception = (Exception) null;
      return SceneTextBox.LoadFromFile(fileName, out obj, out exception);
    }

    public static SceneTextBox LoadFromFile(string fileName)
    {
      FileStream fileStream = (FileStream) null;
      StreamReader streamReader = (StreamReader) null;
      try
      {
        fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
        streamReader = new StreamReader((Stream) fileStream);
        string end = streamReader.ReadToEnd();
        streamReader.Close();
        fileStream.Close();
        return SceneTextBox.Deserialize(end);
      }
      finally
      {
        if (fileStream != null)
          fileStream.Dispose();
        if (streamReader != null)
          streamReader.Dispose();
      }
    }
  }
}
