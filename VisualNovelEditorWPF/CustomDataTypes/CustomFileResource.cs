﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.CustomDataTypes.CustomFileResource
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

namespace VisualNovelEditorWPF.CustomDataTypes
{
  public class CustomFileResource
  {
    public string FileName;
    public string FilePath;

    public CustomFileResource(string tempFileName, string tempFilePath)
    {
      this.FileName = tempFileName;
      this.FilePath = tempFilePath;
    }

    public CustomFileResource(string tempFilePath)
    {
      this.FilePath = tempFilePath;
      string[] strArray = tempFilePath.Split('/');
      this.FileName = strArray[strArray.Length - 1];
    }
  }
}
