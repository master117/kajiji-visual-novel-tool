﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.Scene
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using VisualNovelEditorWPF.CustomDataTypes;

namespace VisualNovelEditorWPF
{
  public class Scene
  {
    private string sceneNameField;
    private SceneStatus statusField;
    private bool statusFieldSpecified;
    private SceneTextBox textBoxField;
    private SceneBackground backgroundField;
    private SceneMusic musicField;
    private List<Sprite> spriteField;
    private string buttonsImageField;
    private List<Button> buttonField;
    private static XmlSerializer serializer;

    public string SceneName
    {
      get
      {
        return this.sceneNameField;
      }
      set
      {
        this.sceneNameField = value;
      }
    }

    public SceneStatus Status
    {
      get
      {
        return this.statusField;
      }
      set
      {
        this.statusField = value;
      }
    }

    [XmlIgnore]
    public bool StatusSpecified
    {
      get
      {
        return this.statusFieldSpecified;
      }
      set
      {
        this.statusFieldSpecified = value;
      }
    }

    public SceneTextBox TextBox
    {
      get
      {
        return this.textBoxField;
      }
      set
      {
        this.textBoxField = value;
      }
    }

    public SceneBackground Background
    {
      get
      {
        return this.backgroundField;
      }
      set
      {
        this.backgroundField = value;
      }
    }

    public SceneMusic Music
    {
      get
      {
        return this.musicField;
      }
      set
      {
        this.musicField = value;
      }
    }

    public List<Sprite> Sprite
    {
      get
      {
        return this.spriteField;
      }
      set
      {
        this.spriteField = value;
      }
    }

    public string ButtonsImage
    {
      get
      {
        return this.buttonsImageField;
      }
      set
      {
        this.buttonsImageField = value;
      }
    }

    public List<Button> Button
    {
      get
      {
        return this.buttonField;
      }
      set
      {
        this.buttonField = value;
      }
    }

    private static XmlSerializer Serializer
    {
      get
      {
        if (Scene.serializer == null)
          Scene.serializer = new XmlSerializer(typeof (Scene));
        return Scene.serializer;
      }
    }

    public Scene()
    {
      this.buttonField = new List<Button>();
      this.spriteField = new List<Sprite>();
      this.musicField = new SceneMusic();
      this.backgroundField = new SceneBackground();
      this.textBoxField = new SceneTextBox();
    }

    public virtual string Serialize()
    {
      StreamReader streamReader = (StreamReader) null;
      MemoryStream memoryStream = (MemoryStream) null;
      try
      {
        memoryStream = new MemoryStream();
        Scene.Serializer.Serialize((Stream) memoryStream, (object) this);
        memoryStream.Seek(0L, SeekOrigin.Begin);
        streamReader = new StreamReader((Stream) memoryStream);
        return streamReader.ReadToEnd();
      }
      finally
      {
        if (streamReader != null)
          streamReader.Dispose();
        if (memoryStream != null)
          memoryStream.Dispose();
      }
    }

    public static bool Deserialize(string xml, out Scene obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (Scene) null;
      try
      {
        obj = Scene.Deserialize(xml);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool Deserialize(string xml, out Scene obj)
    {
      Exception exception = (Exception) null;
      return Scene.Deserialize(xml, out obj, out exception);
    }

    public static Scene Deserialize(string xml)
    {
      StringReader stringReader = (StringReader) null;
      try
      {
        stringReader = new StringReader(xml);
        return (Scene) Scene.Serializer.Deserialize(XmlReader.Create((TextReader) stringReader));
      }
      finally
      {
        if (stringReader != null)
          stringReader.Dispose();
      }
    }

    public virtual bool SaveToFile(string fileName, out Exception exception)
    {
      exception = (Exception) null;
      try
      {
        this.SaveToFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public virtual void SaveToFile(string fileName)
    {
      StreamWriter streamWriter = (StreamWriter) null;
      try
      {
        string str = this.Serialize();
        streamWriter = new FileInfo(fileName).CreateText();
        streamWriter.WriteLine(str);
        streamWriter.Close();
      }
      finally
      {
        if (streamWriter != null)
          streamWriter.Dispose();
      }
    }

    public static bool LoadFromFile(string fileName, out Scene obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (Scene) null;
      try
      {
        obj = Scene.LoadFromFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool LoadFromFile(string fileName, out Scene obj)
    {
      Exception exception = (Exception) null;
      return Scene.LoadFromFile(fileName, out obj, out exception);
    }

    public static Scene LoadFromFile(string fileName)
    {
      FileStream fileStream = (FileStream) null;
      StreamReader streamReader = (StreamReader) null;
      try
      {
        fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
        streamReader = new StreamReader((Stream) fileStream);
        string end = streamReader.ReadToEnd();
        streamReader.Close();
        fileStream.Close();
        return Scene.Deserialize(end);
      }
      finally
      {
        if (fileStream != null)
          fileStream.Dispose();
        if (streamReader != null)
          streamReader.Dispose();
      }
    }
  }
}
