﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.CustomDataTypes.GifImage
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;

namespace VisualNovelEditorWPF.CustomDataTypes
{
  internal class GifImage : Image
  {
    public static readonly DependencyProperty FrameIndexProperty = DependencyProperty.Register("FrameIndex", typeof (int), typeof (GifImage), (PropertyMetadata) new UIPropertyMetadata((object) 0, new PropertyChangedCallback(GifImage.ChangingFrameIndex)));
    public static readonly DependencyProperty AutoStartProperty = DependencyProperty.Register("AutoStart", typeof (bool), typeof (GifImage), (PropertyMetadata) new UIPropertyMetadata((object) false, new PropertyChangedCallback(GifImage.AutoStartPropertyChanged)));
    public static readonly DependencyProperty GifSourceProperty = DependencyProperty.Register("GifSource", typeof (string), typeof (GifImage), (PropertyMetadata) new UIPropertyMetadata((object) string.Empty, new PropertyChangedCallback(GifImage.GifSourcePropertyChanged)));
    private bool _isInitialized;
    private GifBitmapDecoder _gifDecoder;
    private Int32Animation _animation;

    public int FrameIndex
    {
      get
      {
        return (int) this.GetValue(GifImage.FrameIndexProperty);
      }
      set
      {
        this.SetValue(GifImage.FrameIndexProperty, (object) value);
      }
    }

    public bool AutoStart
    {
      get
      {
        return (bool) this.GetValue(GifImage.AutoStartProperty);
      }
      set
      {
        this.SetValue(GifImage.AutoStartProperty, (object) value);
      }
    }

    public string GifSource
    {
      get
      {
        return (string) this.GetValue(GifImage.GifSourceProperty);
      }
      set
      {
        this.SetValue(GifImage.GifSourceProperty, (object) value);
      }
    }

    static GifImage()
    {
      UIElement.VisibilityProperty.OverrideMetadata(typeof (GifImage), (PropertyMetadata) new FrameworkPropertyMetadata(new PropertyChangedCallback(GifImage.VisibilityPropertyChanged)));
    }

    private void Initialize()
    {
      this._gifDecoder = new GifBitmapDecoder(new Uri(this.GifSource, UriKind.RelativeOrAbsolute), BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
      this._animation = new Int32Animation(0, this._gifDecoder.Frames.Count - 1, new Duration(new TimeSpan(0, 0, 0, this._gifDecoder.Frames.Count / 10, (int) (((double) this._gifDecoder.Frames.Count / 10.0 - (double) (this._gifDecoder.Frames.Count / 10)) * 1000.0))));
      this._animation.RepeatBehavior = RepeatBehavior.Forever;
      this.Source = (ImageSource) this._gifDecoder.Frames[0];
      this._isInitialized = true;
    }

    private static void VisibilityPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
      if ((Visibility) e.NewValue == Visibility.Visible)
        ((GifImage) sender).StartAnimation();
      else
        ((GifImage) sender).StopAnimation();
    }

    private static void ChangingFrameIndex(DependencyObject obj, DependencyPropertyChangedEventArgs ev)
    {
      GifImage gifImage = obj as GifImage;
      if (gifImage == null)
        return;
      gifImage.Source = (ImageSource) gifImage._gifDecoder.Frames[(int) ev.NewValue];
    }

    private static void AutoStartPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
      if (!(bool) e.NewValue)
        return;
      GifImage gifImage = (GifImage) sender;
      if (gifImage != null)
        gifImage.StartAnimation();
    }

    private static void GifSourcePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
      if ((GifImage) sender == null)
        return;
      (sender as GifImage).Initialize();
    }

    public void StartAnimation()
    {
      if (!this._isInitialized)
        this.Initialize();
      this.BeginAnimation(GifImage.FrameIndexProperty, (AnimationTimeline) this._animation);
    }

    public void StopAnimation()
    {
      this.BeginAnimation(GifImage.FrameIndexProperty, (AnimationTimeline) null);
    }
  }
}
