﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.Position
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace VisualNovelEditorWPF
{
  public class Position
  {
    private double positionLeftField;
    private double positionTopField;
    private double positionRightField;
    private double positionBottomField;
    private static XmlSerializer serializer;

    public double PositionLeft
    {
      get
      {
        return this.positionLeftField;
      }
      set
      {
        this.positionLeftField = value;
      }
    }

    public double PositionTop
    {
      get
      {
        return this.positionTopField;
      }
      set
      {
        this.positionTopField = value;
      }
    }

    public double PositionRight
    {
      get
      {
        return this.positionRightField;
      }
      set
      {
        this.positionRightField = value;
      }
    }

    public double PositionBottom
    {
      get
      {
        return this.positionBottomField;
      }
      set
      {
        this.positionBottomField = value;
      }
    }

    private static XmlSerializer Serializer
    {
      get
      {
        if (Position.serializer == null)
          Position.serializer = new XmlSerializer(typeof (Position));
        return Position.serializer;
      }
    }

    public virtual string Serialize()
    {
      StreamReader streamReader = (StreamReader) null;
      MemoryStream memoryStream = (MemoryStream) null;
      try
      {
        memoryStream = new MemoryStream();
        Position.Serializer.Serialize((Stream) memoryStream, (object) this);
        memoryStream.Seek(0L, SeekOrigin.Begin);
        streamReader = new StreamReader((Stream) memoryStream);
        return streamReader.ReadToEnd();
      }
      finally
      {
        if (streamReader != null)
          streamReader.Dispose();
        if (memoryStream != null)
          memoryStream.Dispose();
      }
    }

    public static bool Deserialize(string xml, out Position obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (Position) null;
      try
      {
        obj = Position.Deserialize(xml);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool Deserialize(string xml, out Position obj)
    {
      Exception exception = (Exception) null;
      return Position.Deserialize(xml, out obj, out exception);
    }

    public static Position Deserialize(string xml)
    {
      StringReader stringReader = (StringReader) null;
      try
      {
        stringReader = new StringReader(xml);
        return (Position) Position.Serializer.Deserialize(XmlReader.Create((TextReader) stringReader));
      }
      finally
      {
        if (stringReader != null)
          stringReader.Dispose();
      }
    }

    public virtual bool SaveToFile(string fileName, out Exception exception)
    {
      exception = (Exception) null;
      try
      {
        this.SaveToFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public virtual void SaveToFile(string fileName)
    {
      StreamWriter streamWriter = (StreamWriter) null;
      try
      {
        string str = this.Serialize();
        streamWriter = new FileInfo(fileName).CreateText();
        streamWriter.WriteLine(str);
        streamWriter.Close();
      }
      finally
      {
        if (streamWriter != null)
          streamWriter.Dispose();
      }
    }

    public static bool LoadFromFile(string fileName, out Position obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (Position) null;
      try
      {
        obj = Position.LoadFromFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool LoadFromFile(string fileName, out Position obj)
    {
      Exception exception = (Exception) null;
      return Position.LoadFromFile(fileName, out obj, out exception);
    }

    public static Position LoadFromFile(string fileName)
    {
      FileStream fileStream = (FileStream) null;
      StreamReader streamReader = (StreamReader) null;
      try
      {
        fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
        streamReader = new StreamReader((Stream) fileStream);
        string end = streamReader.ReadToEnd();
        streamReader.Close();
        fileStream.Close();
        return Position.Deserialize(end);
      }
      finally
      {
        if (fileStream != null)
          fileStream.Dispose();
        if (streamReader != null)
          streamReader.Dispose();
      }
    }
  }
}
