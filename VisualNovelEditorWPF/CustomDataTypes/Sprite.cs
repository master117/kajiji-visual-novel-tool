﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.Sprite
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using VisualNovelEditorWPF.CustomDataTypes;

namespace VisualNovelEditorWPF
{
  public class Sprite
  {
    private string spriteImagePathField;
    private Position spritePositionField;
    private SpriteFlipped flippedField;
    private bool flippedFieldSpecified;
    private string effectField;
    private string effectStartTypeField;
    private string effectStartTimeField;
    private string effectDurationField;
    private static XmlSerializer serializer;

    public string SpriteImagePath
    {
      get
      {
        return this.spriteImagePathField;
      }
      set
      {
        this.spriteImagePathField = value;
      }
    }

    public Position SpritePosition
    {
      get
      {
        return this.spritePositionField;
      }
      set
      {
        this.spritePositionField = value;
      }
    }

    public SpriteFlipped Flipped
    {
      get
      {
        return this.flippedField;
      }
      set
      {
        this.flippedField = value;
      }
    }

    [XmlIgnore]
    public bool FlippedSpecified
    {
      get
      {
        return this.flippedFieldSpecified;
      }
      set
      {
        this.flippedFieldSpecified = value;
      }
    }

    public string Effect
    {
      get
      {
        return this.effectField;
      }
      set
      {
        this.effectField = value;
      }
    }

    public string EffectStartType
    {
      get
      {
        return this.effectStartTypeField;
      }
      set
      {
        this.effectStartTypeField = value;
      }
    }

    public string EffectStartTime
    {
      get
      {
        return this.effectStartTimeField;
      }
      set
      {
        this.effectStartTimeField = value;
      }
    }

    public string EffectDuration
    {
      get
      {
        return this.effectDurationField;
      }
      set
      {
        this.effectDurationField = value;
      }
    }

    private static XmlSerializer Serializer
    {
      get
      {
        if (Sprite.serializer == null)
          Sprite.serializer = new XmlSerializer(typeof (Sprite));
        return Sprite.serializer;
      }
    }

    public Sprite()
    {
      this.spritePositionField = new Position();
    }

    public virtual string Serialize()
    {
      StreamReader streamReader = (StreamReader) null;
      MemoryStream memoryStream = (MemoryStream) null;
      try
      {
        memoryStream = new MemoryStream();
        Sprite.Serializer.Serialize((Stream) memoryStream, (object) this);
        memoryStream.Seek(0L, SeekOrigin.Begin);
        streamReader = new StreamReader((Stream) memoryStream);
        return streamReader.ReadToEnd();
      }
      finally
      {
        if (streamReader != null)
          streamReader.Dispose();
        if (memoryStream != null)
          memoryStream.Dispose();
      }
    }

    public static bool Deserialize(string xml, out Sprite obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (Sprite) null;
      try
      {
        obj = Sprite.Deserialize(xml);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool Deserialize(string xml, out Sprite obj)
    {
      Exception exception = (Exception) null;
      return Sprite.Deserialize(xml, out obj, out exception);
    }

    public static Sprite Deserialize(string xml)
    {
      StringReader stringReader = (StringReader) null;
      try
      {
        stringReader = new StringReader(xml);
        return (Sprite) Sprite.Serializer.Deserialize(XmlReader.Create((TextReader) stringReader));
      }
      finally
      {
        if (stringReader != null)
          stringReader.Dispose();
      }
    }

    public virtual bool SaveToFile(string fileName, out Exception exception)
    {
      exception = (Exception) null;
      try
      {
        this.SaveToFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public virtual void SaveToFile(string fileName)
    {
      StreamWriter streamWriter = (StreamWriter) null;
      try
      {
        string str = this.Serialize();
        streamWriter = new FileInfo(fileName).CreateText();
        streamWriter.WriteLine(str);
        streamWriter.Close();
      }
      finally
      {
        if (streamWriter != null)
          streamWriter.Dispose();
      }
    }

    public static bool LoadFromFile(string fileName, out Sprite obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (Sprite) null;
      try
      {
        obj = Sprite.LoadFromFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool LoadFromFile(string fileName, out Sprite obj)
    {
      Exception exception = (Exception) null;
      return Sprite.LoadFromFile(fileName, out obj, out exception);
    }

    public static Sprite LoadFromFile(string fileName)
    {
      FileStream fileStream = (FileStream) null;
      StreamReader streamReader = (StreamReader) null;
      try
      {
        fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
        streamReader = new StreamReader((Stream) fileStream);
        string end = streamReader.ReadToEnd();
        streamReader.Close();
        fileStream.Close();
        return Sprite.Deserialize(end);
      }
      finally
      {
        if (fileStream != null)
          fileStream.Dispose();
        if (streamReader != null)
          streamReader.Dispose();
      }
    }
  }
}
