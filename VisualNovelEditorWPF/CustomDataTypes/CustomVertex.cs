﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.CustomDataTypes.CustomVertex
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System.Diagnostics;

namespace VisualNovelEditorWPF.CustomDataTypes
{
  [DebuggerDisplay("{ID}")]
  internal class CustomVertex
  {
    public string ID { get; private set; }

    public bool IsTemporary { get; private set; }

    public Scene DedicatedScene { get; private set; }

    public CustomVertex(Scene tempDedicatedScene, bool tempIsTemporary)
    {
      this.ID = tempDedicatedScene.SceneName;
      this.IsTemporary = tempIsTemporary;
      this.DedicatedScene = tempDedicatedScene;
    }

    public override string ToString()
    {
      if (this.IsTemporary)
        return this.ID + " (Temporary)";
      return this.ID;
    }
  }
}
