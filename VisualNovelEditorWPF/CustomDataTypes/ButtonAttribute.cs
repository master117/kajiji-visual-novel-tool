﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.ButtonAttribute
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace VisualNovelEditorWPF
{
  public class ButtonAttribute
  {
    private string attributeNameField;
    private string valueRequiredField;
    private string valueChangeField;
    private static XmlSerializer serializer;

    public string AttributeName
    {
      get
      {
        return this.attributeNameField;
      }
      set
      {
        this.attributeNameField = value;
      }
    }

    public string ValueRequired
    {
      get
      {
        return this.valueRequiredField;
      }
      set
      {
        this.valueRequiredField = value;
      }
    }

    public string ValueChange
    {
      get
      {
        return this.valueChangeField;
      }
      set
      {
        this.valueChangeField = value;
      }
    }

    private static XmlSerializer Serializer
    {
      get
      {
        if (ButtonAttribute.serializer == null)
          ButtonAttribute.serializer = new XmlSerializer(typeof (ButtonAttribute));
        return ButtonAttribute.serializer;
      }
    }

    public virtual string Serialize()
    {
      StreamReader streamReader = (StreamReader) null;
      MemoryStream memoryStream = (MemoryStream) null;
      try
      {
        memoryStream = new MemoryStream();
        ButtonAttribute.Serializer.Serialize((Stream) memoryStream, (object) this);
        memoryStream.Seek(0L, SeekOrigin.Begin);
        streamReader = new StreamReader((Stream) memoryStream);
        return streamReader.ReadToEnd();
      }
      finally
      {
        if (streamReader != null)
          streamReader.Dispose();
        if (memoryStream != null)
          memoryStream.Dispose();
      }
    }

    public static bool Deserialize(string xml, out ButtonAttribute obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (ButtonAttribute) null;
      try
      {
        obj = ButtonAttribute.Deserialize(xml);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool Deserialize(string xml, out ButtonAttribute obj)
    {
      Exception exception = (Exception) null;
      return ButtonAttribute.Deserialize(xml, out obj, out exception);
    }

    public static ButtonAttribute Deserialize(string xml)
    {
      StringReader stringReader = (StringReader) null;
      try
      {
        stringReader = new StringReader(xml);
        return (ButtonAttribute) ButtonAttribute.Serializer.Deserialize(XmlReader.Create((TextReader) stringReader));
      }
      finally
      {
        if (stringReader != null)
          stringReader.Dispose();
      }
    }

    public virtual bool SaveToFile(string fileName, out Exception exception)
    {
      exception = (Exception) null;
      try
      {
        this.SaveToFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public virtual void SaveToFile(string fileName)
    {
      StreamWriter streamWriter = (StreamWriter) null;
      try
      {
        string str = this.Serialize();
        streamWriter = new FileInfo(fileName).CreateText();
        streamWriter.WriteLine(str);
        streamWriter.Close();
      }
      finally
      {
        if (streamWriter != null)
          streamWriter.Dispose();
      }
    }

    public static bool LoadFromFile(string fileName, out ButtonAttribute obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (ButtonAttribute) null;
      try
      {
        obj = ButtonAttribute.LoadFromFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool LoadFromFile(string fileName, out ButtonAttribute obj)
    {
      Exception exception = (Exception) null;
      return ButtonAttribute.LoadFromFile(fileName, out obj, out exception);
    }

    public static ButtonAttribute LoadFromFile(string fileName)
    {
      FileStream fileStream = (FileStream) null;
      StreamReader streamReader = (StreamReader) null;
      try
      {
        fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
        streamReader = new StreamReader((Stream) fileStream);
        string end = streamReader.ReadToEnd();
        streamReader.Close();
        fileStream.Close();
        return ButtonAttribute.Deserialize(end);
      }
      finally
      {
        if (fileStream != null)
          fileStream.Dispose();
        if (streamReader != null)
          streamReader.Dispose();
      }
    }
  }
}
