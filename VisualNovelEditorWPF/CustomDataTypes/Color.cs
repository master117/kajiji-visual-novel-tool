﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.Color
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace VisualNovelEditorWPF
{
  public class Color
  {
    private byte redField;
    private bool redFieldSpecified;
    private byte greenField;
    private bool greenFieldSpecified;
    private byte blueField;
    private bool blueFieldSpecified;
    private static XmlSerializer serializer;

    public byte Red
    {
      get
      {
        return this.redField;
      }
      set
      {
        this.redField = value;
      }
    }

    [XmlIgnore]
    public bool RedSpecified
    {
      get
      {
        return this.redFieldSpecified;
      }
      set
      {
        this.redFieldSpecified = value;
      }
    }

    public byte Green
    {
      get
      {
        return this.greenField;
      }
      set
      {
        this.greenField = value;
      }
    }

    [XmlIgnore]
    public bool GreenSpecified
    {
      get
      {
        return this.greenFieldSpecified;
      }
      set
      {
        this.greenFieldSpecified = value;
      }
    }

    public byte Blue
    {
      get
      {
        return this.blueField;
      }
      set
      {
        this.blueField = value;
      }
    }

    [XmlIgnore]
    public bool BlueSpecified
    {
      get
      {
        return this.blueFieldSpecified;
      }
      set
      {
        this.blueFieldSpecified = value;
      }
    }

    private static XmlSerializer Serializer
    {
      get
      {
        if (Color.serializer == null)
          Color.serializer = new XmlSerializer(typeof (Color));
        return Color.serializer;
      }
    }

    public virtual string Serialize()
    {
      StreamReader streamReader = (StreamReader) null;
      MemoryStream memoryStream = (MemoryStream) null;
      try
      {
        memoryStream = new MemoryStream();
        Color.Serializer.Serialize((Stream) memoryStream, (object) this);
        memoryStream.Seek(0L, SeekOrigin.Begin);
        streamReader = new StreamReader((Stream) memoryStream);
        return streamReader.ReadToEnd();
      }
      finally
      {
        if (streamReader != null)
          streamReader.Dispose();
        if (memoryStream != null)
          memoryStream.Dispose();
      }
    }

    public static bool Deserialize(string xml, out Color obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (Color) null;
      try
      {
        obj = Color.Deserialize(xml);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool Deserialize(string xml, out Color obj)
    {
      Exception exception = (Exception) null;
      return Color.Deserialize(xml, out obj, out exception);
    }

    public static Color Deserialize(string xml)
    {
      StringReader stringReader = (StringReader) null;
      try
      {
        stringReader = new StringReader(xml);
        return (Color) Color.Serializer.Deserialize(XmlReader.Create((TextReader) stringReader));
      }
      finally
      {
        if (stringReader != null)
          stringReader.Dispose();
      }
    }

    public virtual bool SaveToFile(string fileName, out Exception exception)
    {
      exception = (Exception) null;
      try
      {
        this.SaveToFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public virtual void SaveToFile(string fileName)
    {
      StreamWriter streamWriter = (StreamWriter) null;
      try
      {
        string str = this.Serialize();
        streamWriter = new FileInfo(fileName).CreateText();
        streamWriter.WriteLine(str);
        streamWriter.Close();
      }
      finally
      {
        if (streamWriter != null)
          streamWriter.Dispose();
      }
    }

    public static bool LoadFromFile(string fileName, out Color obj, out Exception exception)
    {
      exception = (Exception) null;
      obj = (Color) null;
      try
      {
        obj = Color.LoadFromFile(fileName);
        return true;
      }
      catch (Exception ex)
      {
        exception = ex;
        return false;
      }
    }

    public static bool LoadFromFile(string fileName, out Color obj)
    {
      Exception exception = (Exception) null;
      return Color.LoadFromFile(fileName, out obj, out exception);
    }

    public static Color LoadFromFile(string fileName)
    {
      FileStream fileStream = (FileStream) null;
      StreamReader streamReader = (StreamReader) null;
      try
      {
        fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
        streamReader = new StreamReader((Stream) fileStream);
        string end = streamReader.ReadToEnd();
        streamReader.Close();
        fileStream.Close();
        return Color.Deserialize(end);
      }
      finally
      {
        if (fileStream != null)
          fileStream.Dispose();
        if (streamReader != null)
          streamReader.Dispose();
      }
    }
  }
}
