﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.CreateNovelUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class CreateNovelUserControl : UserControl, IComponentConnector
  {
    public static event CreateNovelUserControl.createNovelDelegate createNovel;

    public static event CreateNovelUserControl.backButtonDelegate backButton;

    public CreateNovelUserControl()
    {
      this.InitializeComponent();
    }

    private void CreateNovelGridCreateButton_Click(object sender, RoutedEventArgs e)
    {
      bool? isChecked;
      int num1;
      if (this.CreateNovelGridNovelTypeStandardNovelRadioButton.IsChecked.HasValue)
      {
        isChecked = this.CreateNovelGridNovelTypeStandardNovelRadioButton.IsChecked;
        num1 = !isChecked.Value ? 1 : 0;
      }
      else
        num1 = 1;
      if (num1 == 0)
      {
        CreateNovelUserControl.createNovel(this.CreateNovelGridNovelNameTextBox.Text, VisualNovelVisualNovelType.Standard);
      }
      else
      {
        isChecked = this.CreateNovelGridNovelTypeSimNovelRadioButton.IsChecked;
        int num2;
        if (isChecked.HasValue)
        {
          isChecked = this.CreateNovelGridNovelTypeSimNovelRadioButton.IsChecked;
          num2 = !isChecked.Value ? 1 : 0;
        }
        else
          num2 = 1;
        if (num2 != 0)
          return;
        CreateNovelUserControl.createNovel(this.CreateNovelGridNovelNameTextBox.Text, VisualNovelVisualNovelType.Sim);
      }
    }

    private void BackButton_Click(object sender, RoutedEventArgs e)
    {
      CreateNovelUserControl.backButton();
    }

    

    public delegate void createNovelDelegate(string tempNovelname, VisualNovelVisualNovelType tempVisualNovelType);

    public delegate void backButtonDelegate();
  }
}
