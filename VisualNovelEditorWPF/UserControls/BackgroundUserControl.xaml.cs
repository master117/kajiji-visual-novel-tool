﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.BackgroundUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using VisualNovelEditorWPF.CustomDataTypes;
using VisualNovelEditorWPF.Properties;
using VisualNovelEditorWPF.Static;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class BackgroundUserControl : UserControl, IComponentConnector
  {
    private CustomFileResource backgroundBackgroundCustomFileResource;


    public static event BackgroundUserControl.backgroundUpdateDelegate backgroundUpdate;

    public BackgroundUserControl()
    {
      this.InitializeComponent();
    }

    public SceneBackground GetBackground()
    {
      SceneBackground SceneBackground = new SceneBackground();
      if (this.backgroundBackgroundCustomFileResource != null)
        SceneBackground.BackgroundImagePath = this.backgroundBackgroundCustomFileResource.FileName;
      return SceneBackground;
    }

    internal SceneBackground GetPreviewBackground()
    {
      SceneBackground background = this.GetBackground();
      if (this.backgroundBackgroundCustomFileResource != null)
        background.BackgroundImagePath = this.backgroundBackgroundCustomFileResource.FilePath;
      return background;
    }

    public CustomFileResource GetCustomFileResource()
    {
      return this.backgroundBackgroundCustomFileResource;
    }

    private void BrowseComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ComboBox comboBox = (ComboBox) sender;
      if (comboBox.SelectedIndex != 2)
        return;
      OpenFileDialog openFileDialog = new OpenFileDialog();
      try
      {
        openFileDialog.Filter = "Image (.jpg, .png, .bmp, .jpeg, .gif)|*.jpg; *.png; *.bmp; *.jpeg; *.gif";
        openFileDialog.ShowDialog();
        string str = openFileDialog.SafeFileName;
        if (str.Length > 15)
          str = str.Substring(0, 15);
        if (str == "")
        {
          if (comboBox.Items.Count > 3)
            comboBox.Items.Remove(comboBox.Items[3]);
          comboBox.SelectedItem = comboBox.Items[0];
          return;
        }
        if (comboBox.Items.Count > 3)
          comboBox.Items.Remove(comboBox.Items[3]);
        comboBox.Items.Add((object) str);
        comboBox.SelectedItem = comboBox.Items[3];
        try
        {
          this.backgroundBackgroundCustomFileResource = new CustomFileResource(openFileDialog.SafeFileName, openFileDialog.FileName);
        }
        catch (ArgumentException ex)
        {
          return;
        }
      }
      catch (Exception ex)
      {
        return;
      }
      BackgroundUserControl.backgroundUpdate();
    }

    public ImageSource GetBackgroundImageSource()
    {
      return new ImageBrush((ImageSource) BitmapHandler.getBitmapImage(this.backgroundBackgroundCustomFileResource.FilePath)).ImageSource;
    }

    public void ApplyVisualNovelTemplate(Scene tempVisualNovelTemplate)
    {
      this.LoadBackground(Settings.Default.TemplateDirectory + tempVisualNovelTemplate.SceneName + Settings.Default.ResourcesDirectory + tempVisualNovelTemplate.Background.BackgroundImagePath);
      BackgroundUserControl.backgroundUpdate();
    }

    internal void LoadScene(Scene tempScene)
    {
      this.LoadBackground(Settings.Default.VisualNovelDirectory + VisualNovelHandler.mainVisualNovel.VisualNovelName + Settings.Default.ResourcesDirectory + tempScene.Background.BackgroundImagePath);
    }

    private void LoadBackground(string tempBackgroundFilePath)
    {
      string[] strArray = tempBackgroundFilePath.Split('/');
      string tempFileName = strArray[strArray.Length - 1];
      string str = tempFileName;
      if (str.Length > 15)
        str = str.Substring(0, 15);
      if (this.BrowseComboBox.Items.Count > 3)
        this.BrowseComboBox.Items.Remove(this.BrowseComboBox.Items[3]);
      this.BrowseComboBox.Items.Add((object) str);
      this.BrowseComboBox.SelectedItem = this.BrowseComboBox.Items[3];
      this.backgroundBackgroundCustomFileResource = new CustomFileResource(tempFileName, tempBackgroundFilePath);
    }

    

    public delegate void backgroundUpdateDelegate();
  }
}
