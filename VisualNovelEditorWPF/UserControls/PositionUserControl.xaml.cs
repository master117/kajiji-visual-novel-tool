﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.PositionUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using VisualNovelEditorWPF.Static;
using Xceed.Wpf.Toolkit;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class PositionUserControl : UserControl, IComponentConnector
  {
    private Position standardPosition;

    public static event PositionUserControl.refreshPositionDelegate refreshPosition;

    public PositionUserControl()
    {
      this.InitializeComponent();
      this.standardPosition = StaticMethods.StandardTextBoxPosition;
    }

    public Position GetPosition()
    {
      if (!this.CustomRadioButton.IsChecked.HasValue || !this.CustomRadioButton.IsChecked.Value)
        return this.standardPosition;
      try
      {
        return new Position()
        {
          PositionLeft = double.Parse(this.LeftBorderUpDown.Text),
          PositionTop = double.Parse(this.TopBorderUpDown.Text),
          PositionRight = double.Parse(this.RightBorderUpDown.Text),
          PositionBottom = double.Parse(this.BottomBorderUpDown.Text)
        };
      }
      catch (Exception ex)
      {
        return this.standardPosition;
      }
    }

    public void SetPosition(Position tempPosition)
    {
      this.LeftBorderUpDown.Text = tempPosition.PositionLeft.ToString();
      this.TopBorderUpDown.Text = tempPosition.PositionTop.ToString();
      this.RightBorderUpDown.Text = tempPosition.PositionRight.ToString();
      this.BottomBorderUpDown.Text = tempPosition.PositionBottom.ToString();
      this.CustomRadioButton.IsChecked = new bool?(true);
      PositionUserControl.refreshPosition();
    }

    public void SetStandardPosition(Position tempPosition)
    {
      this.standardPosition = tempPosition;
    }

    private void TextBoxRefreshButton_Click(object sender, RoutedEventArgs e)
    {
      PositionUserControl.refreshPosition();
    }

    
    public delegate void refreshPositionDelegate();
  }
}
