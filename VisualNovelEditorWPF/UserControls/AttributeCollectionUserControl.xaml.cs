﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.AttributeCollectionUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class AttributeCollectionUserControl : UserControl, IComponentConnector
  {
    private readonly List<AttributeUserControl> mainAttributeUserControlList = new List<AttributeUserControl>();


    public static event AttributeCollectionUserControl.saveAttributesButtonDelegate saveAttributesButton;

    public AttributeCollectionUserControl()
    {
      this.InitializeComponent();
    }

    public List<Attribute> GetAttributes()
    {
      List<Attribute> tAttributeList = new List<Attribute>();
      foreach (AttributeUserControl attributeUserControl in this.mainAttributeUserControlList)
        tAttributeList.Add(attributeUserControl.GetAttribute());
      return tAttributeList;
    }

    private void AttributeCollectionAddButton_Click(object sender, RoutedEventArgs e)
    {
      AttributeUserControl attributeUserControl = new AttributeUserControl();
      this.mainAttributeUserControlList.Add(attributeUserControl);
      this.AttributeStackPanel.Children.Add((UIElement) attributeUserControl);
    }

    private void CreateAttributesGridButton_Click(object sender, RoutedEventArgs e)
    {
      AttributeCollectionUserControl.saveAttributesButton();
    }

    public delegate void saveAttributesButtonDelegate();
  }
}
