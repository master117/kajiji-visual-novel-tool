﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.ButtonAttributeUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class ButtonAttributeUserControl : UserControl, IComponentConnector
  {
    public ButtonAttributeUserControl(List<string> attributeNameStringList)
    {
      this.InitializeComponent();
      this.ButtonAttributeNameComboBox.ItemsSource = (IEnumerable) attributeNameStringList;
    }

    public ButtonAttribute GetButtonAttribute()
    {
      return new ButtonAttribute()
      {
        AttributeName = this.ButtonAttributeNameComboBox.SelectedItem.ToString(),
        ValueRequired = this.ButtonAttributeRequieredTextBox.Text,
        ValueChange = this.ButtonAttributeChangeTextBox.Text
      };
    }

    internal void LoadAttribute(ButtonAttribute tempButtonAttribute)
    {
      this.ButtonAttributeNameComboBox.SelectedValue = (object) tempButtonAttribute.AttributeName;
      this.ButtonAttributeRequieredTextBox.Text = tempButtonAttribute.ValueRequired;
      this.ButtonAttributeChangeTextBox.Text = tempButtonAttribute.ValueChange;
    }

    
  }
}
