﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.ButtonUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using VisualNovelEditorWPF.Static;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class ButtonUserControl : UserControl, IComponentConnector
  {
    private readonly List<ButtonAttributeUserControl> mainButtonAttributeUserControlList = new List<ButtonAttributeUserControl>();


    public static event ButtonUserControl.deleteButtonDelegate deleteButton;

    public ButtonUserControl()
    {
      this.InitializeComponent();
      if (VisualNovelHandler.GetNovelType() != VisualNovelVisualNovelType.Sim)
        return;
      this.ButtonAttributeCollectionUserControlInstance.Visibility = Visibility.Visible;
    }

    public Button GetButton()
    {
      Button button = new Button();
      if (string.IsNullOrEmpty(this.ButtonNextTextBox.Text))
        return (Button) null;
      button.ButtonText = this.ButtonTextTextBox.Text;
      button.NextSceneName = this.ButtonNextTextBox.Text;
      List<ButtonAttribute> tButtonAttributeList = new List<ButtonAttribute>();
      foreach (ButtonAttributeUserControl attributeUserControl in this.mainButtonAttributeUserControlList)
        tButtonAttributeList.Add(attributeUserControl.GetButtonAttribute());
      button.ButtonAttribute = tButtonAttributeList;
      return button;
    }

    private void RemoveButtonButton_Click(object sender, RoutedEventArgs e)
    {
      ButtonUserControl.deleteButton(this);
    }

    internal void SetNextSceneName(string nextSceneName)
    {
      this.ButtonNextTextBox.Text = nextSceneName;
    }

    internal void LoadButton(Button tempButton)
    {
      this.ButtonNextTextBox.Text = tempButton.NextSceneName;
      this.ButtonTextTextBox.Text = tempButton.ButtonText;
      if (tempButton.ButtonAttribute == null)
        return;
      this.ButtonAttributeCollectionUserControlInstance.LoadAttributes(tempButton.ButtonAttribute);
    }

    public delegate void deleteButtonDelegate(ButtonUserControl thisObject);
  }
}
