﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.TextBoxUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using VisualNovelEditorWPF.CustomDataTypes;
using VisualNovelEditorWPF.Properties;
using VisualNovelEditorWPF.Static;
using Xceed.Wpf.Toolkit;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class TextBoxUserControl : UserControl, IComponentConnector
  {
    private bool reachedButtons = false;
    private FontFamily currentFontFamily = new FontFamily("Century Gothic");
    private double currentFontSize = 26.0;
    private System.Windows.Media.Color currentFontColor = Colors.Black;
    private readonly List<TextBox> mainTextBoxList = new List<TextBox>();
    private TextBox currentTextBox = new TextBox();
    private CustomFileResource textBoxBackgroundCustomFileResource;
    
    public static event TextBoxUserControl.textBoxUpdateDelegate textBoxUpdate;

    public static event TextBoxUserControl.textBoxButtonClickedDelegate textBoxButtonClicked;

    public TextBoxUserControl()
    {
      this.InitializeComponent();
      List<int> intList = new List<int>();
      int num = 12;
      while (num <= 72)
      {
        intList.Add(num);
        num += 4;
      }
      this.FontSizeComboBox.ItemsSource = (IEnumerable) intList;
      this.mainTextBoxList.Add(this.GetStandardTextBox());
      this.TextBoxGrid.Children.Clear();
      this.TextBoxGrid.Children.Add((UIElement) this.currentTextBox);
      this.currentTextBox = this.mainTextBoxList[0];
      this.PositionUserControlInstance.SetStandardPosition(StaticMethods.StandardTextBoxPosition);
      this.RefreshTextBox();
    }

    public void StepTextBox(int steps)
    {
      if (steps > 0)
      {
        for (int index = 0; index < steps; ++index)
          this.IncreaseTextBoxCounter();
      }
      if (steps < 0)
      {
        for (int index = 0; index > steps; --index)
          this.DecreaseTextBoxCounter();
      }
      this.UpdateAllTextBoxUIElements();
    }

    private void DecreaseTextBoxCounter()
    {
      if (this.reachedButtons)
      {
        this.reachedButtons = false;
      }
      else
      {
        if (this.mainTextBoxList.IndexOf(this.currentTextBox) == 0)
          return;
        this.currentTextBox = this.mainTextBoxList[this.mainTextBoxList.IndexOf(this.currentTextBox) - 1];
      }
    }

    private void IncreaseTextBoxCounter()
    {
      if (this.mainTextBoxList.IndexOf(this.currentTextBox) == this.mainTextBoxList.Count - 1)
        this.reachedButtons = true;
      else
        this.currentTextBox = this.mainTextBoxList[this.mainTextBoxList.IndexOf(this.currentTextBox) + 1];
    }

    private void InsertNewTextbox(int index)
    {
      this.mainTextBoxList.Insert(index, this.GetStandardTextBox());
    }

    private void RemoveTextBox(TextBox textBoxToRemove)
    {
      if (this.mainTextBoxList.IndexOf(textBoxToRemove) != 0)
        this.StepTextBox(-1);
      else
        this.mainTextBoxList[0].Text = "";
      this.mainTextBoxList.Remove(textBoxToRemove);
    }

    public void UpdateAllTextBoxUIElements()
    {
      this.RefreshTextBox();
      TextBoxUserControl.textBoxUpdate();
    }

    public void RefreshTextBox()
    {
      this.TextBoxGrid.Children.Clear();
      this.TextBoxGrid.Children.Add((UIElement) this.currentTextBox);
    }

    private TextBox GetStandardTextBox()
    {
      TextBox textBox = new TextBox();
      textBox.Height = 200.0;
      textBox.Width = 350.0;
      textBox.FontSize = 14.0;
      textBox.FontFamily = new FontFamily("Century Gothic");
      textBox.BorderThickness = new Thickness(3.0);
      return textBox;
    }

    public List<string> GetTextBoxContent()
    {
      List<string> stringList = new List<string>();
      foreach (TextBox mainTextBox in this.mainTextBoxList)
        stringList.Add(mainTextBox.Text);
      return stringList;
    }

    public SceneTextBox GetTextBox()
    {
      SceneTextBox sceneTextBox = new SceneTextBox();
      if (this.textBoxBackgroundCustomFileResource != null)
        sceneTextBox.TextBoxImagePath = this.textBoxBackgroundCustomFileResource.FileName;
      sceneTextBox.Speaker = this.SpeakerNameTextBox.Text;
      sceneTextBox.Text = this.GetTextBoxContent();
      sceneTextBox.TextBoxPosition = this.GetTPosition();
      return sceneTextBox;
    }

    public SceneTextBox GetPreviewTextBox()
    {
      SceneTextBox textBox = this.GetTextBox();
      if (this.textBoxBackgroundCustomFileResource != null)
        textBox.TextBoxImagePath = this.textBoxBackgroundCustomFileResource.FilePath;
      return textBox;
    }

    public ImageSource GetTextBoxImageSource()
    {
      if (this.textBoxBackgroundCustomFileResource != null)
        return new ImageBrush((ImageSource) BitmapHandler.getBitmapImage(this.textBoxBackgroundCustomFileResource.FilePath)).ImageSource;
      return (ImageSource) null;
    }

    public System.Windows.Media.Color GetFontColor()
    {
      return this.currentFontColor;
    }

    public double GetFontSize()
    {
      return this.currentFontSize;
    }

    public FontFamily GetFont()
    {
      return this.currentFontFamily;
    }

    public CustomFileResource GetCustomFileResource()
    {
      return this.textBoxBackgroundCustomFileResource;
    }

    internal Position GetTPosition()
    {
      return this.PositionUserControlInstance.GetPosition();
    }

    private void TextBoxContentLeftArrowButton_Click(object sender, RoutedEventArgs e)
    {
      TextBoxUserControl.textBoxButtonClicked(-1);
    }

    private void TextBoxContentRightArrowButton_Click(object sender, RoutedEventArgs e)
    {
      TextBoxUserControl.textBoxButtonClicked(1);
    }

    private void BrowseComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ComboBox comboBox = (ComboBox) sender;
      if (comboBox.SelectedIndex != 2)
        return;
      OpenFileDialog openFileDialog = new OpenFileDialog();
      try
      {
        openFileDialog.Filter = "Image (.jpg, .png, .bmp, .jpeg, .gif)|*.jpg; *.png; *.bmp; *.jpeg; *.gif";
        openFileDialog.ShowDialog();
        string str = openFileDialog.SafeFileName;
        if (str.Length > 15)
          str = str.Substring(0, 15);
        if (str == "")
        {
          if (comboBox.Items.Count > 3)
            comboBox.Items.Remove(comboBox.Items[3]);
          comboBox.SelectedItem = comboBox.Items[0];
          return;
        }
        if (comboBox.Items.Count > 3)
          comboBox.Items.Remove(comboBox.Items[3]);
        comboBox.Items.Add((object) str);
        comboBox.SelectedItem = comboBox.Items[3];
        try
        {
          this.textBoxBackgroundCustomFileResource = new CustomFileResource(openFileDialog.SafeFileName, openFileDialog.FileName);
        }
        catch (ArgumentException ex)
        {
        }
      }
      catch (Exception ex)
      {
      }
      TextBoxUserControl.textBoxUpdate();
    }

    private void TextBoxRefreshButton_Click(object sender, RoutedEventArgs e)
    {
      this.UpdateAllTextBoxUIElements();
    }

    private void TextBoxDeleteButton_Click(object sender, RoutedEventArgs e)
    {
      this.RemoveTextBox(this.currentTextBox);
      this.UpdateAllTextBoxUIElements();
    }

    private void TextBoxAddButton_Click(object sender, RoutedEventArgs e)
    {
      if (this.reachedButtons)
        TextBoxUserControl.textBoxButtonClicked(-1);
      this.InsertNewTextbox(this.mainTextBoxList.IndexOf(this.currentTextBox) + 1);
      TextBoxUserControl.textBoxButtonClicked(1);
    }

    private void FontSizeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      int result;
      if (this.FontSizeComboBox.SelectedValue == null || !int.TryParse(this.FontSizeComboBox.SelectedValue.ToString(), out result) || result <= 0)
        return;
      this.currentFontSize = (double) result;
      this.UpdateAllTextBoxUIElements();
    }

    private void FontColorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<System.Windows.Media.Color> e)
    {
      this.currentFontColor = this.FontColorPicker.SelectedColor;
      TextBoxUserControl.textBoxUpdate();
    }

    private void FontComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.FontComboBox.SelectedItem == null)
        return;
      this.currentFontFamily = (FontFamily) this.FontComboBox.SelectedItem;
      this.currentTextBox.FontFamily = (FontFamily) this.FontComboBox.SelectedItem;
      this.UpdateAllTextBoxUIElements();
    }

    internal void LoadScene(Scene tempScene)
    {
      try
      {
        this.SpeakerNameTextBox.Text = tempScene.TextBox.Speaker;
        this.SetTextBoxesContent(tempScene.TextBox.Text.ToArray());
        this.FontSizeComboBox.SelectedValue = (object) tempScene.TextBox.FontSize;
        if (tempScene.TextBox.TextBoxImagePath != null)
          this.LoadBackground(Settings.Default.VisualNovelDirectory + VisualNovelHandler.mainVisualNovel.VisualNovelName + Settings.Default.ResourcesDirectory + tempScene.TextBox.TextBoxImagePath);
        this.SetTextBoxPosition(tempScene.TextBox.TextBoxPosition);
        if (tempScene.TextBox.Color != null)
          this.FontColorPicker.SelectedColor = System.Windows.Media.Color.FromRgb(tempScene.TextBox.Color.Red, tempScene.TextBox.Color.Green, tempScene.TextBox.Color.Blue);
        if (tempScene.TextBox.Decoration != null)
        {
          this.FontDecorationComboBox.Items.Add((object) tempScene.TextBox.Decoration);
          this.FontDecorationComboBox.SelectedIndex = this.FontDecorationComboBox.Items.Count - 1;
        }
        if (tempScene.TextBox.Font == null)
          return;
        this.FontComboBox.Items.Add((object) new FontFamily(tempScene.TextBox.Font));
        this.FontComboBox.SelectedIndex = this.FontComboBox.Items.Count - 1;
      }
      catch (Exception ex)
      {
      }
    }

    private void LoadBackground(string tempBackgroundFilePath)
    {
      string[] strArray = tempBackgroundFilePath.Split('/');
      string tempFileName = strArray[strArray.Length - 1];
      string str = tempFileName;
      if (str.Length > 15)
        str = str.Substring(0, 15);
      if (this.BrowseComboBox.Items.Count > 3)
        this.BrowseComboBox.Items.Remove(this.BrowseComboBox.Items[3]);
      this.BrowseComboBox.Items.Add((object) str);
      this.BrowseComboBox.SelectedItem = this.BrowseComboBox.Items[3];
      this.textBoxBackgroundCustomFileResource = new CustomFileResource(tempFileName, tempBackgroundFilePath);
    }

    private void SetTextBoxPosition(Position tempPosition)
    {
      this.PositionUserControlInstance.SetPosition(tempPosition);
    }

    private void SetTextBoxesContent(string[] tempTextBoxesContentList)
    {
      this.TextBoxGrid.Children.Clear();
      this.mainTextBoxList.Clear();
      foreach (string textBoxesContent in tempTextBoxesContentList)
      {
        TextBox standardTextBox = this.GetStandardTextBox();
        standardTextBox.Text = textBoxesContent;
        this.mainTextBoxList.Add(standardTextBox);
      }
    }

    public void ApplyVisualNovelTemplate(Scene tempVisualNovelTemplate)
    {
      this.LoadBackground(Settings.Default.TemplateDirectory + tempVisualNovelTemplate.SceneName + Settings.Default.ResourcesDirectory + tempVisualNovelTemplate.TextBox.TextBoxImagePath);
      this.SetTextBoxPosition(tempVisualNovelTemplate.TextBox.TextBoxPosition);
      TextBoxUserControl.textBoxUpdate();
    }

    

    public delegate void textBoxUpdateDelegate();

    public delegate void textBoxButtonClickedDelegate(int step);
  }
}
