﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.PreviewSideUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using VisualNovelEditorWPF.CustomDataTypes;
using VisualNovelEditorWPF.Static;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class PreviewSideUserControl : UserControl, IComponentConnector
  {
    private bool reachedButtons = false;
    private int textBoxPanelCounter = 0;
    private readonly Random randomByte = new Random();
    private string[] textBoxContent;
    private bool BordersVisibility;
    public Scene CurrentScene;
    private bool mouseDownInToolbar;
    private Point DragOffset;

    public static event PreviewSideUserControl.spriteUpdatedDelegate spriteUpdated;

    public static event PreviewSideUserControl.textBoxClickedDelegate textBoxClicked;

    public PreviewSideUserControl()
    {
      this.InitializeComponent();
    }

    public void StepTextBox(int steps)
    {
      if (steps > 0)
      {
        for (int index = 0; index < steps; ++index)
          this.IncreaseTextBoxCounter();
      }
      if (steps < 0)
      {
        for (int index = 0; index > steps; --index)
          this.DecreaseTextBoxCounter();
      }
      this.TextBoxTextBlock.Text = this.textBoxContent[this.textBoxPanelCounter];
    }

    private void DecreaseTextBoxCounter()
    {
      if (this.reachedButtons)
      {
        this.reachedButtons = false;
        this.ShowTextBox();
      }
      else
      {
        if (this.textBoxPanelCounter == 0)
          return;
        --this.textBoxPanelCounter;
      }
    }

    private void IncreaseTextBoxCounter()
    {
      if (this.textBoxPanelCounter == this.textBoxContent.Length - 1)
      {
        this.reachedButtons = true;
        this.ShowButtons();
      }
      else
        ++this.textBoxPanelCounter;
    }

    public void ShowTextBox()
    {
      this.TextBoxTextBlock.Visibility = Visibility.Visible;
      this.ButtonStackPanel.Visibility = Visibility.Collapsed;
    }

    public void ShowButtons()
    {
      this.TextBoxTextBlock.Visibility = Visibility.Collapsed;
      this.ButtonStackPanel.Visibility = Visibility.Visible;
    }

    internal void ShowBorders(bool showBorders)
    {
      this.BordersVisibility = showBorders;
      this.RefreshBorders();
    }

    public bool IsSceneSet()
    {
      return this.CurrentScene != null;
    }

    public void SetScene(Scene tempScene)
    {
      this.CurrentScene = tempScene;
    }

    public void Update()
    {
      this.VerifyAccess();
      if (this.CurrentScene == null)
        return;
      this.UpdateBackgroundBackground();
      this.UpdateSprites();
      this.UpdateTextBox();
      this.UpdateTextBoxContent();
      this.UpdateButtons();
    }

    private void UpdateBackgroundBackground()
    {
      if (this.CurrentScene.Background.BackgroundImagePath == null)
        return;
      this.BackgroundImage.BeginInit();
      this.BackgroundImage.Source = (ImageSource) BitmapHandler.getBitmapImage(this.CurrentScene.Background.BackgroundImagePath);
      this.BackgroundImage.EndInit();
    }

    private void UpdateSprites()
    {
      this.SpriteSpaceGrid.Children.Clear();
      foreach (Sprite tSprite in this.CurrentScene.Sprite)
      {
        if (tSprite != null && tSprite.SpriteImagePath != null)
        {
          Border tempInnerControl = new Border();
          tempInnerControl.Padding = new Thickness(0.0);
          tempInnerControl.BorderThickness = new Thickness(0.0);
          tempInnerControl.Background = (Brush) new SolidColorBrush(Colors.Transparent);
          tempInnerControl.MouseRightButtonDown += new MouseButtonEventHandler(this.RightClickBackgroundImage);
          tempInnerControl.MouseLeftButtonDown += new MouseButtonEventHandler(this.OnSpriteBorderClicked);
          tempInnerControl.MouseLeftButtonUp += new MouseButtonEventHandler(this.OnSpriteBorderReleased);
          tempInnerControl.MouseMove += new MouseEventHandler(this.OnSpriteBorderMoving);
          if (!tSprite.SpriteImagePath.EndsWith(".gif"))
          {
            Image tempImage = new Image();
            tempImage.BeginInit();
            tempImage.Source = (ImageSource) BitmapHandler.getBitmapImage(tSprite.SpriteImagePath);
            tempImage.EndInit();
            tempImage.Margin = new Thickness(0.0);
            tempImage.Stretch = Stretch.Fill;
            if (tSprite.Flipped == SpriteFlipped.Flipped)
              StaticMethods.FlipImageVertical(tempImage);
            tempInnerControl.Tag = (object) tSprite;
            tempInnerControl.Child = (UIElement) tempImage;
          }
          else
          {
            GifImage gifImage = new GifImage();
            gifImage.GifSource = tSprite.SpriteImagePath;
            gifImage.VerticalAlignment = VerticalAlignment.Top;
            gifImage.HorizontalAlignment = HorizontalAlignment.Left;
            gifImage.Stretch = Stretch.Fill;
            gifImage.AutoStart = true;
            if (tSprite.Flipped == SpriteFlipped.Flipped)
              StaticMethods.FlipImageVertical((Image) gifImage);
            tempInnerControl.Tag = (object) tSprite;
            tempInnerControl.Child = (UIElement) gifImage;
          }
          StaticMethods.UpdateControlPosition(tempInnerControl, this.PreviewSideGrid, tSprite.SpritePosition);
          this.SpriteSpaceGrid.Children.Add((UIElement) tempInnerControl);
        }
      }
    }

    private void UpdateTextBox()
    {
      if (this.CurrentScene.TextBox.TextBoxPosition != null)
        StaticMethods.UpdateControlPosition((Control) this.TextBoxTextBlock, this.PreviewSideGrid, this.CurrentScene.TextBox.TextBoxPosition);
      if (this.CurrentScene.TextBox.TextBoxImagePath == null)
        return;
      this.TextBoxTextBlock.Background = (Brush) new ImageBrush((ImageSource) BitmapHandler.getBitmapImage(this.CurrentScene.TextBox.TextBoxImagePath));
    }

    public void UpdateTextBoxContent()
    {
      this.textBoxContent = this.CreateTextBoxContent(this.CurrentScene.TextBox.Text, "You");
      this.StepTextBox(0);
    }

    public string[] CreateTextBoxContent(List<string> tempTextList, string tempSpeakerName)
    {
      return tempTextList.ToArray();
    }

    private void UpdateButtons()
    {
      this.ButtonStackPanel.Children.Clear();
      foreach (Button tButton in this.CurrentScene.Button)
      {
        System.Windows.Controls.Button button = new System.Windows.Controls.Button();
        button.Content = (object) tButton.ButtonText;
        button.Tag = (object) tButton.NextSceneName;
        button.Background = (Brush) new SolidColorBrush(Colors.LightGoldenrodYellow);
        button.Margin = new Thickness(0.0, 30.0, 0.0, 30.0);
        button.MouseRightButtonDown += new MouseButtonEventHandler(this.tempButton_MouseRightButtonDown);
        button.MinWidth = 200.0;
        button.MinHeight = 30.0;
        this.ButtonStackPanel.Children.Add((UIElement) button);
      }
    }

    public void RefreshBorders()
    {
      if (this.BordersVisibility)
      {
        this.TextBoxTextBlock.BorderThickness = new Thickness(1.0);
        foreach (Border child in this.SpriteSpaceGrid.Children)
        {
          byte r = (byte) this.randomByte.Next(256);
          byte g = (byte) this.randomByte.Next(256);
          byte b = (byte) this.randomByte.Next(256);
          child.Background = (Brush) new SolidColorBrush(System.Windows.Media.Color.FromRgb(r, g, b));
          child.Opacity = 0.9;
        }
      }
      else
      {
        this.TextBoxTextBlock.BorderThickness = new Thickness(0.0);
        foreach (Border child in this.SpriteSpaceGrid.Children)
        {
          child.Background = (Brush) new SolidColorBrush(Colors.Transparent);
          child.Opacity = 1.0;
        }
      }
    }

    private void LeftClickTextBlock(object sender, MouseButtonEventArgs e)
    {
      PreviewSideUserControl.textBoxClicked(1);
    }

    private void RightClickTextBlock(object sender, MouseButtonEventArgs e)
    {
      PreviewSideUserControl.textBoxClicked(-1);
    }

    private void tempButton_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
    {
      PreviewSideUserControl.textBoxClicked(-1);
    }

    private void RightClickBackgroundImage(object sender, MouseButtonEventArgs e)
    {
      if (this.reachedButtons)
        this.ButtonStackPanel.Visibility = Visibility.Collapsed;
      else
        this.TextBoxTextBlock.Visibility = Visibility.Collapsed;
    }

    private void OnSpriteBorderClicked(object sender, MouseButtonEventArgs e)
    {
      Border border = (Border) sender;
      byte r = (byte) this.randomByte.Next(256);
      byte g = (byte) this.randomByte.Next(256);
      byte b = (byte) this.randomByte.Next(256);
      border.Background = (Brush) new SolidColorBrush(System.Windows.Media.Color.FromRgb(r, g, b));
      border.Opacity = 0.9;
      this.mouseDownInToolbar = true;
      this.DragOffset = e.GetPosition((IInputElement) this.PreviewSideGrid);
      border.CaptureMouse();
    }

    private void OnSpriteBorderReleased(object sender, MouseButtonEventArgs e)
    {
      Border tempInnerControl = (Border) sender;
      tempInnerControl.Background = (Brush) new SolidColorBrush(Colors.Transparent);
      tempInnerControl.Opacity = 1.0;
      this.mouseDownInToolbar = false;
      tempInnerControl.ReleaseMouseCapture();
      ((Sprite) tempInnerControl.Tag).SpritePosition = StaticMethods.GetControlTPosition(tempInnerControl, this.PreviewSideGrid);
      PreviewSideUserControl.spriteUpdated(this.CurrentScene.Sprite);
    }

    private void OnSpriteBorderMoving(object sender, MouseEventArgs e)
    {
      if (!this.mouseDownInToolbar)
        return;
      Border border = (Border) sender;
      TranslateTransform tempUserControlTranslateTransform = new TranslateTransform();
      border.RenderTransform = (Transform) tempUserControlTranslateTransform;
      this.MoveUserControl(tempUserControlTranslateTransform, e);
    }

    private void MoveUserControl(TranslateTransform tempUserControlTranslateTransform, MouseEventArgs e)
    {
      Point position = e.GetPosition((IInputElement) this.PreviewSideGrid);
      double num1 = tempUserControlTranslateTransform.X + (position.X - this.DragOffset.X);
      double num2 = tempUserControlTranslateTransform.Y + (position.Y - this.DragOffset.Y);
      tempUserControlTranslateTransform.X = num1;
      tempUserControlTranslateTransform.Y = num2;
    }

    

    public delegate void spriteUpdatedDelegate(List<Sprite> tempTSpriteList);

    public delegate void textBoxClickedDelegate(int step);
  }
}
