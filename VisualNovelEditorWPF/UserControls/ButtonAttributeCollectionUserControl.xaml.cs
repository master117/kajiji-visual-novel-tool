﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.ButtonAttributeCollectionUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using VisualNovelEditorWPF.Static;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class ButtonAttributeCollectionUserControl : UserControl, IComponentConnector
  {
    private readonly List<ButtonAttributeUserControl> mainButtonAttributeUserControlList = new List<ButtonAttributeUserControl>();
    private readonly List<string> mainButtonAttributeNameList = new List<string>();


    public ButtonAttributeCollectionUserControl()
    {
      this.InitializeComponent();
      List<Attribute> attributes = VisualNovelHandler.GetAttributes();
      List<string> stringList = new List<string>();
      foreach (Attribute tAttribute in attributes)
        stringList.Add(tAttribute.AttributeName);
      this.mainButtonAttributeNameList = stringList;
    }

    private void AttributeCollectionAddButton_Click(object sender, RoutedEventArgs e)
    {
      ButtonAttributeUserControl attributeUserControl = new ButtonAttributeUserControl(this.mainButtonAttributeNameList);
      this.mainButtonAttributeUserControlList.Add(attributeUserControl);
      this.ButtonAttributeStackPanel.Children.Add((UIElement) attributeUserControl);
    }

    internal void LoadAttributes(List<ButtonAttribute> tButtonAttributeList)
    {
      foreach (ButtonAttribute tButtonAttribute in tButtonAttributeList)
      {
        ButtonAttributeUserControl attributeUserControl = new ButtonAttributeUserControl(this.mainButtonAttributeNameList);
        attributeUserControl.LoadAttribute(tButtonAttribute);
        this.mainButtonAttributeUserControlList.Add(attributeUserControl);
        this.ButtonAttributeStackPanel.Children.Add((UIElement) attributeUserControl);
      }
    }

    internal List<ButtonAttribute> GetAttributes()
    {
      List<ButtonAttribute> tButtonAttributeList = new List<ButtonAttribute>();
      foreach (ButtonAttributeUserControl attributeUserControl in this.mainButtonAttributeUserControlList)
        tButtonAttributeList.Add(attributeUserControl.GetButtonAttribute());
      return tButtonAttributeList;
    }

    
  }
}
