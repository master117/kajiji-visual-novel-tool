﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.SpriteCollectionUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class SpriteCollectionUserControl : UserControl, IComponentConnector
  {
    private readonly List<SpriteUserControl> mainSpriteUserControlList = new List<SpriteUserControl>();

    public SpriteCollectionUserControl()
    {
      this.InitializeComponent();
      SpriteUserControl.deleteSprite += new SpriteUserControl.deleteSpriteDelegate(this.SpriteUserControl_deleteSprite);
    }

    public List<SpriteUserControl> GetSpriteUserControlList()
    {
      return this.mainSpriteUserControlList;
    }

    public List<Sprite> GetTSpriteList()
    {
      List<Sprite> tSpriteList = new List<Sprite>();
      foreach (SpriteUserControl spriteUserControl in this.mainSpriteUserControlList)
      {
        Sprite tsprite = spriteUserControl.GetTSprite();
        if (tsprite != null)
          tSpriteList.Add(tsprite);
      }
      return tSpriteList;
    }

    internal List<Sprite> GetPreviewTSpriteList()
    {
      List<Sprite> tSpriteList = new List<Sprite>();
      foreach (SpriteUserControl spriteUserControl in this.mainSpriteUserControlList)
      {
        Sprite previewTsprite = spriteUserControl.GetPreviewTSprite();
        if (previewTsprite != null)
          tSpriteList.Add(previewTsprite);
      }
      return tSpriteList;
    }

    private void SpriteCollectionAddButton_Click(object sender, RoutedEventArgs e)
    {
      SpriteUserControl spriteUserControl = new SpriteUserControl();
      this.mainSpriteUserControlList.Add(spriteUserControl);
      this.SpriteCollectionSpriteStackPanel.Children.Add((UIElement) spriteUserControl);
    }

    internal void SetSprites(List<Sprite> tempTSpriteList)
    {
      this.mainSpriteUserControlList.Clear();
      this.SpriteCollectionSpriteStackPanel.Children.Clear();
      foreach (Sprite tempTsprite in tempTSpriteList)
      {
        SpriteUserControl spriteUserControl = new SpriteUserControl();
        spriteUserControl.SetSprite(tempTsprite);
        this.mainSpriteUserControlList.Add(spriteUserControl);
        this.SpriteCollectionSpriteStackPanel.Children.Add((UIElement) spriteUserControl);
      }
    }

    private void SpriteUserControl_deleteSprite(SpriteUserControl invokingSpriteUserControl)
    {
      this.mainSpriteUserControlList.Remove(invokingSpriteUserControl);
      this.SpriteCollectionSpriteStackPanel.Children.Remove((UIElement) invokingSpriteUserControl);
    }

    internal void LoadScene(Scene tempScene)
    {
      this.SetSprites(tempScene.Sprite);
    }

    
  }
}
