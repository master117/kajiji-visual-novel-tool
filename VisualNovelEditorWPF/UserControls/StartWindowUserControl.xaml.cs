﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.StartWindowUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class StartWindowUserControl : UserControl, IComponentConnector
  {


    public static event StartWindowUserControl.newNovelButtonDelegate newNovelButton;

    public static event StartWindowUserControl.loadNovelButtonDelegate loadNovelButton;

    public StartWindowUserControl()
    {
      this.InitializeComponent();
    }

    private void StartWindowGridNewVisualNovelButton_Click(object sender, RoutedEventArgs e)
    {
      StartWindowUserControl.newNovelButton();
    }

    private void StartWindowGridLoadNovelButton_Click(object sender, RoutedEventArgs e)
    {
      StartWindowUserControl.loadNovelButton();
    }

    

    public delegate void newNovelButtonDelegate();

    public delegate void loadNovelButtonDelegate();
  }
}
