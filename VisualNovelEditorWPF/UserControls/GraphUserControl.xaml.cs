﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.GraphUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using GraphSharp.Controls;
using QuickGraph;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using VisualNovelEditorWPF.CustomDataTypes;
using VisualNovelEditorWPF.Static;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class GraphUserControl : UserControl, IComponentConnector, IStyleConnector
  {
    private static IBidirectionalGraph<object, IEdge<object>> mainIGraph;
    private VertexControl lastClickedVertexControl;

    public static event GraphUserControl.loadSceneDelegate loadScene;

    public static event GraphUserControl.addButtonDelegate addButton;

    public GraphUserControl()
    {
      this.InitializeComponent();
      this.CreateGraph();
      this.AddGraph();
    }

    public void CreateGraph()
    {
      BidirectionalGraph<object, IEdge<object>> bidirectionalGraph = new BidirectionalGraph<object, IEdge<object>>();
      List<Scene> scenes = VisualNovelHandler.GetScenes();
      if (scenes != null)
      {
        Dictionary<string, CustomVertex> dictionary = new Dictionary<string, CustomVertex>();
        foreach (Scene tempDedicatedScene in scenes)
        {
          if (tempDedicatedScene.Status == SceneStatus.Temporary)
            dictionary.Add(tempDedicatedScene.SceneName, new CustomVertex(tempDedicatedScene, true));
          else
            dictionary.Add(tempDedicatedScene.SceneName, new CustomVertex(tempDedicatedScene, false));
        }
        foreach (CustomVertex customVertex in dictionary.Values)
          bidirectionalGraph.AddVertex((object) customVertex);
        foreach (Scene tScene in scenes)
        {
          foreach (Button tButton in tScene.Button)
          {
            if (dictionary.ContainsKey(tScene.SceneName) && dictionary.ContainsKey(tButton.NextSceneName))
            {
              bidirectionalGraph.AddEdge((IEdge<object>) new CustomEdge<object>(tButton.ButtonText, (object) dictionary[tScene.SceneName], (object) dictionary[tButton.NextSceneName]));
            }
            else
            {
              int num = (int) MessageBox.Show("There seems to be an inconsisty in your visual novel, Scene " + tScene.SceneName + " links to scene " + tButton.NextSceneName + ". Are both Scenes available and written correct, if not manually edit the VN");
            }
          }
        }
      }
      GraphUserControl.mainIGraph = (IBidirectionalGraph<object, IEdge<object>>) bidirectionalGraph;
    }

    public void AddGraph()
    {
      this.MainGraphLayout.Graph = GraphUserControl.mainIGraph;
    }

    private void VertexControl_MouseButtonDown(object sender, MouseButtonEventArgs e)
    {
      VertexControl vertexControl = (VertexControl) sender;
      if (this.lastClickedVertexControl != null)
      {
        if (!vertexControl.Equals((object) this.lastClickedVertexControl))
        {
          CustomVertex vertex1 = (CustomVertex) this.lastClickedVertexControl.Vertex;
          CustomVertex vertex2 = (CustomVertex) vertexControl.Vertex;
          GraphUserControl.addButton(vertex1.DedicatedScene, vertex2.DedicatedScene);
          this.CreateGraph();
          this.AddGraph();
          this.lastClickedVertexControl = (VertexControl) null;
        }
        else
        {
          this.lastClickedVertexControl.BorderBrush = (Brush) null;
          this.lastClickedVertexControl.BorderThickness = new Thickness(0.0);
          this.lastClickedVertexControl = (VertexControl) null;
        }
      }
      else
      {
        this.lastClickedVertexControl = vertexControl;
        vertexControl.BorderBrush = (Brush) new SolidColorBrush(Colors.GreenYellow);
        vertexControl.BorderThickness = new Thickness(2.0);
      }
    }

    private void VertexControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      CustomVertex vertex = (CustomVertex) ((VertexControl) sender).Vertex;
      this.lastClickedVertexControl = (VertexControl) null;
      GraphUserControl.loadScene(vertex.DedicatedScene);
    }

    private void EdgeControl_MouseButtonDown(object sender, MouseButtonEventArgs e)
    {
      throw new NotImplementedException();
    }

    private void EdgeControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      throw new NotImplementedException();
    }

    

    public delegate void loadSceneDelegate(Scene tempScene);

    public delegate void addButtonDelegate(Scene sourceScene, Scene targetScene);

      public void Connect(int connectionId, object target)
      {
          throw new NotImplementedException();
      }
  }
}
