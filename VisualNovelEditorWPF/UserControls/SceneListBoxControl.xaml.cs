﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.SceneListBoxControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using VisualNovelEditorWPF.Static;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class SceneListBoxControl : UserControl, IComponentConnector
  {
    public SceneListBoxControl()
    {
      this.InitializeComponent();
    }

    public void UpdateContent()
    {
      this.MainListBox.Items.Clear();
      if (VisualNovelHandler.GetScenes() == null)
        return;
      foreach (Scene scene in VisualNovelHandler.GetScenes())
      {
        foreach (Button tButton in scene.Button)
        {
          ListBoxItem listBoxItem = new ListBoxItem();
          listBoxItem.Content = (object) (scene.SceneName + "  --" + tButton.ButtonText + "-->  " + tButton.NextSceneName);
          this.MainListBox.Items.Add((object) listBoxItem);
        }
      }
    }

    
  }
}
