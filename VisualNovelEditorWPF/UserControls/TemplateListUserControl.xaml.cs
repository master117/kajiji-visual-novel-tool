﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.TemplateListUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using VisualNovelEditorWPF.Properties;
using VisualNovelEditorWPF.Static;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class TemplateListUserControl : UserControl, IComponentConnector
  {
    private readonly List<TemplateUserControl> templateList = new List<TemplateUserControl>();

    public TemplateListUserControl()
    {
      this.InitializeComponent();
    }

    public void UpdateTemplates()
    {
      this.templateList.Clear();
      this.TemplateStackPanel.Children.Clear();
      if (Directory.Exists(Settings.Default.TemplateDirectory) && Directory.GetFiles(Settings.Default.TemplateDirectory, "*.vnt").Length > 0)
      {
        foreach (string file in Directory.GetFiles(Settings.Default.TemplateDirectory, "*.vnt"))
        {
          Scene templateFromFile = VisualNovelTemplateHandler.GetTemplateFromFile(file);
          if (templateFromFile != null)
          {
            TemplateUserControl templateUserControl = new TemplateUserControl(templateFromFile);
            templateUserControl.TemplateNameLabel.Content = (object) templateFromFile.SceneName;
            if (templateFromFile.Background.BackgroundImagePath != null)
              templateUserControl.BackgroundImage.Source = (ImageSource) VisualNovelTemplateHandler.GetBitmapImage(Settings.Default.TemplateDirectory + templateFromFile.SceneName + Settings.Default.ResourcesDirectory + templateFromFile.Background.BackgroundImagePath);
            if (templateFromFile.TextBox.TextBoxImagePath != null)
              templateUserControl.TextBoxImage.Source = (ImageSource) VisualNovelTemplateHandler.GetBitmapImage(Settings.Default.TemplateDirectory + templateFromFile.SceneName + Settings.Default.ResourcesDirectory + templateFromFile.TextBox.TextBoxImagePath);
            if (templateFromFile.TextBox.TextBoxPosition != null)
              StaticMethods.UpdateControlPositionFixedSizeGrid(templateUserControl.TextBoxImage, templateUserControl.ImageGrid, templateFromFile.TextBox.TextBoxPosition);
            else
              StaticMethods.UpdateControlPositionFixedSizeGrid(templateUserControl.TextBoxImage, templateUserControl.ImageGrid, StaticMethods.StandardTextBoxPosition);
            this.templateList.Add(templateUserControl);
            this.TemplateStackPanel.Children.Add((UIElement) templateUserControl);
          }
        }
      }
      else
      {
        TemplateUserControl templateUserControl = new TemplateUserControl((Scene) null);
        this.templateList.Add(templateUserControl);
        this.TemplateStackPanel.Children.Add((UIElement) templateUserControl);
      }
    }

    
  }
}
