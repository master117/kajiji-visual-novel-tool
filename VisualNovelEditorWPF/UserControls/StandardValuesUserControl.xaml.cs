﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.StandardValuesUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class StandardValuesUserControl : UserControl, IComponentConnector
  {


    public StandardValuesUserControl()
    {
      this.InitializeComponent();
    }

    internal void ResetScene()
    {
      this.SceneNameTextBox.Text = "";
    }

    internal void LoadScene(Scene tempScene)
    {
      this.SceneNameTextBox.Text = tempScene.SceneName;
    }

    public bool IsStartScene()
    {
      return this.StartSceneCheckBox.IsChecked.HasValue && this.StartSceneCheckBox.IsChecked.Value;
    }

    public string GetSceneName()
    {
      return this.SceneNameTextBox.Text;
    }

    internal void SetSceneName(string nextSceneName)
    {
      this.SceneNameTextBox.Text = nextSceneName;
    }

    
  }
}
