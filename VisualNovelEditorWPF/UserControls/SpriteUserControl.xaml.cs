﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.SpriteUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using VisualNovelEditorWPF.CustomDataTypes;
using VisualNovelEditorWPF.Properties;
using VisualNovelEditorWPF.Static;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class SpriteUserControl : UserControl, IComponentConnector
  {
    private CustomFileResource spriteCustomFileResource;

    public static event SpriteUserControl.spriteUpdateDelegate spriteUpdate;

    public static event SpriteUserControl.deleteSpriteDelegate deleteSprite;

    public SpriteUserControl()
    {
      this.InitializeComponent();
      this.PositionUserControlInstance.SetStandardPosition(StaticMethods.StandardSpritePosition);
    }

    internal Sprite GetTSprite()
    {
      if (this.spriteCustomFileResource == null)
        return (Sprite) null;
      return new Sprite()
      {
        SpriteImagePath = this.spriteCustomFileResource.FileName,
        SpritePosition = this.GetSpritePosition(),
        Flipped = !this.IsFlipped() ? SpriteFlipped.Standard : SpriteFlipped.Flipped
      };
    }

    internal Sprite GetPreviewTSprite()
    {
      if (this.spriteCustomFileResource == null)
        return (Sprite) null;
      Sprite tsprite = this.GetTSprite();
      tsprite.SpriteImagePath = this.spriteCustomFileResource.FilePath;
      return tsprite;
    }

    public Position GetSpritePosition()
    {
      return this.PositionUserControlInstance.GetPosition();
    }

    public CustomFileResource GetCustomFileResource()
    {
      return this.spriteCustomFileResource;
    }

    public bool IsFlipped()
    {
      return this.FlippedCheckBox.IsChecked.HasValue && this.FlippedCheckBox.IsChecked.Value;
    }

    internal void SetSprite(Sprite tempSprite)
    {
      this.SetSpriteTPosition(tempSprite.SpritePosition);
      if (tempSprite.Flipped == SpriteFlipped.Flipped)
        this.FlippedCheckBox.IsChecked = new bool?(true);
      else
        this.FlippedCheckBox.IsChecked = new bool?(false);
      this.SetSpriteImage(tempSprite.SpriteImagePath);
    }

    private void SetSpriteImage(string tempSpriteFilePath)
    {
      string[] strArray = tempSpriteFilePath.Split('/');
      string tempFileName = strArray[strArray.Length - 1];
      string str = tempFileName;
      if (str.Length > 15)
        str = str.Substring(0, 15);
      if (this.BrowseComboBox.Items.Count > 3)
        this.BrowseComboBox.Items.Remove(this.BrowseComboBox.Items[3]);
      this.BrowseComboBox.Items.Add((object) str);
      this.BrowseComboBox.SelectedItem = this.BrowseComboBox.Items[3];
      this.spriteCustomFileResource = new CustomFileResource(tempFileName, tempSpriteFilePath);
    }

    public void SetSpriteTPosition(Position tempPosition)
    {
      this.PositionUserControlInstance.SetPosition(tempPosition);
    }

    private void BrowseComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ComboBox comboBox = (ComboBox) sender;
      if (comboBox.SelectedIndex != 2)
        return;
      OpenFileDialog openFileDialog = new OpenFileDialog();
      try
      {
        openFileDialog.Filter = "Image (.jpg, .png, .bmp, .jpeg, .gif)|*.jpg; *.png; *.bmp; *.jpeg; *.gif";
        openFileDialog.ShowDialog();
        if (comboBox.Items.Count > 3)
          comboBox.Items.Remove(comboBox.Items[3]);
        comboBox.Items.Add((object) openFileDialog.SafeFileName);
        comboBox.SelectedItem = comboBox.Items[3];
        try
        {
          this.spriteCustomFileResource = new CustomFileResource(openFileDialog.SafeFileName, openFileDialog.FileName);
        }
        catch (ArgumentException ex)
        {
        }
      }
      catch (Exception ex)
      {
      }
      SpriteUserControl.spriteUpdate();
    }

    private void FlippedCheckBox_Click(object sender, RoutedEventArgs e)
    {
      SpriteUserControl.spriteUpdate();
    }

    private void RemoveButton_Click(object sender, RoutedEventArgs e)
    {
      SpriteUserControl.deleteSprite(this);
      SpriteUserControl.spriteUpdate();
    }

    internal void LoadSprite(Sprite tempSprite)
    {
      tempSprite.SpriteImagePath = Settings.Default.VisualNovelDirectory + VisualNovelHandler.mainVisualNovel.VisualNovelName + Settings.Default.ResourcesDirectory + tempSprite.SpriteImagePath;
      this.SetSprite(tempSprite);
    }

    

    public delegate void spriteUpdateDelegate();

    public delegate void deleteSpriteDelegate(SpriteUserControl thisObject);
  }
}
