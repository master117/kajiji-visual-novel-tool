﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.TemplateUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class TemplateUserControl : UserControl, IComponentConnector
  {
    private readonly Scene mainVisualNovelTemplate;

    public static event TemplateUserControl.templateClickedDelegate loadTemplate;

    public TemplateUserControl(Scene tempVisualNovelTemplate)
    {
      this.InitializeComponent();
      this.mainVisualNovelTemplate = tempVisualNovelTemplate;
    }

    private void TemplateButton_Click(object sender, RoutedEventArgs e)
    {
      if (this.mainVisualNovelTemplate == null)
        return;
      TemplateUserControl.loadTemplate(this.mainVisualNovelTemplate);
    }

    

    public delegate void templateClickedDelegate(Scene visualNovelTemplate);
  }
}
