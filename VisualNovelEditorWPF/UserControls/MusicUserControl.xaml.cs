﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.MusicUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using VisualNovelEditorWPF.CustomDataTypes;
using VisualNovelEditorWPF.Properties;
using VisualNovelEditorWPF.Static;
using Xceed.Wpf.Toolkit;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class MusicUserControl : UserControl, IComponentConnector
  {
    private CustomFileResource musicCustomFileRessource;

    public MusicUserControl()
    {
      this.InitializeComponent();
      this.RepeatComboBox.SelectedIndex = 0;
      this.MusicStartTypeComboBox.SelectedIndex = 0;
    }

    private void ShowAdditionalOptions(bool showBool)
    {
      if (showBool)
      {
        this.MusicStartTimeDockPanel.Visibility = Visibility.Visible;
        this.MusicDurationDockPanel.Visibility = Visibility.Visible;
      }
      else
      {
        this.MusicStartTimeDockPanel.Visibility = Visibility.Collapsed;
        this.MusicDurationDockPanel.Visibility = Visibility.Collapsed;
      }
    }

    private void BrowseComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ComboBox comboBox = (ComboBox) sender;
      if (comboBox.SelectedIndex != 2)
        return;
      OpenFileDialog openFileDialog = new OpenFileDialog();
      try
      {
        openFileDialog.Filter = "Music (.mp3, .wav, .wma, .aiff)|*.mp3; *.wav; *.wma; *.aiff;";
        openFileDialog.ShowDialog();
        if (openFileDialog.SafeFileName == "")
        {
          if (comboBox.Items.Count > 3)
            comboBox.Items.Remove(comboBox.Items[3]);
          comboBox.SelectedItem = comboBox.Items[0];
          return;
        }
        if (comboBox.Items.Count > 3)
          comboBox.Items.Remove(comboBox.Items[3]);
        comboBox.Items.Add((object) openFileDialog.SafeFileName);
        comboBox.SelectedItem = comboBox.Items[3];
        try
        {
          this.musicCustomFileRessource = new CustomFileResource(openFileDialog.SafeFileName, openFileDialog.FileName);
        }
        catch (ArgumentException ex)
        {
          return;
        }
      }
      catch (Exception ex)
      {
        return;
      }
      if (this.musicCustomFileRessource != null)
        this.SoundChanged();
    }

    private void MusicStartTypeComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (this.MusicStartTypeComboBox.SelectedIndex > 0)
        this.ShowAdditionalOptions(true);
      else
        this.ShowAdditionalOptions(false);
    }

    public SceneMusic GetMusic()
    {
      SceneMusic sceneMusic = new SceneMusic();
      if (this.musicCustomFileRessource != null)
        sceneMusic.MusicFilePath = this.musicCustomFileRessource.FileName;
      switch (this.RepeatComboBox.SelectedIndex)
      {
        case 0:
          sceneMusic.MusicRepeat = MusicRepeat.Standard;
          break;
        case 1:
          sceneMusic.MusicRepeat = MusicRepeat.Repeat;
          break;
      }
      switch (this.MusicStartTypeComboBox.SelectedIndex)
      {
        case 0:
          sceneMusic.MusicStartType = MusicStartType.Instant;
          break;
        case 1:
          sceneMusic.MusicStartType = MusicStartType.Clicks;
          break;
        case 2:
          sceneMusic.MusicStartType = MusicStartType.Seconds;
          break;
      }
      if (!string.IsNullOrEmpty(this.MusicStartTimeTextBox.Text))
      {
        try
        {
          sceneMusic.MusicStartTime = int.Parse(this.MusicStartTimeTextBox.Text);
        }
        catch (Exception ex)
        {
        }
      }
      if (!string.IsNullOrEmpty(this.MusicDurationTextBox.Text))
      {
        try
        {
          sceneMusic.MusicDuration = int.Parse(this.MusicDurationTextBox.Text);
        }
        catch (Exception ex)
        {
        }
      }
      return sceneMusic;
    }

    public CustomFileResource GetCustomFileResource()
    {
      if (this.musicCustomFileRessource != null)
        return this.musicCustomFileRessource;
      return (CustomFileResource) null;
    }

    internal void LoadScene(Scene tempScene)
    {
      if (tempScene.Music.MusicFilePath != null)
        this.SetMusicCustomFileResource(Settings.Default.VisualNovelDirectory + VisualNovelHandler.mainVisualNovel.VisualNovelName + Settings.Default.ResourcesDirectory + tempScene.Music.MusicFilePath);
      switch (tempScene.Music.MusicRepeat)
      {
        case MusicRepeat.Standard:
          this.RepeatComboBox.SelectedIndex = 0;
          break;
        case MusicRepeat.Repeat:
          this.RepeatComboBox.SelectedIndex = 1;
          break;
      }
      switch (tempScene.Music.MusicStartType)
      {
        case MusicStartType.Instant:
          this.MusicStartTypeComboBox.SelectedIndex = 0;
          break;
        case MusicStartType.Clicks:
          this.MusicStartTypeComboBox.SelectedIndex = 2;
          break;
        case MusicStartType.Seconds:
          this.MusicStartTypeComboBox.SelectedIndex = 1;
          break;
      }
      WatermarkTextBox startTimeTextBox = this.MusicStartTimeTextBox;
      int num = tempScene.Music.MusicStartTime;
      string str1 = num.ToString();
      startTimeTextBox.Text = str1;
      WatermarkTextBox musicDurationTextBox = this.MusicDurationTextBox;
      num = tempScene.Music.MusicDuration;
      string str2 = num.ToString();
      musicDurationTextBox.Text = str2;
    }

    private void SetMusicCustomFileResource(string tempMusicFilePath)
    {
      this.musicCustomFileRessource = new CustomFileResource(tempMusicFilePath);
      string str = this.musicCustomFileRessource.FileName;
      if (str.Length > 15)
        str = str.Substring(0, 15);
      if (this.BrowseComboBox.Items.Count > 3)
        this.BrowseComboBox.Items.Remove(this.BrowseComboBox.Items[3]);
      this.BrowseComboBox.Items.Add((object) str);
      this.BrowseComboBox.SelectedItem = this.BrowseComboBox.Items[3];
    }

    public void PlaySound()
    {
      if (this.GetCustomFileResource() == null)
        return;
      SoundPlayerHandler.ChangeSong(this.GetCustomFileResource().FilePath);
      SoundPlayerHandler.TurnOn();
    }

    public void StopSound()
    {
      SoundPlayerHandler.TurnOff();
    }

    public void SoundChanged()
    {
      if (!SoundPlayerHandler.CheckStatus())
        return;
      this.PlaySound();
    }

    
  }
}
