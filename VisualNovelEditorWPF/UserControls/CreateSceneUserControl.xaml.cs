﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.CreateSceneUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using VisualNovelEditorWPF.CustomDataTypes;
using VisualNovelEditorWPF.Static;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class CreateSceneUserControl : UserControl, IComponentConnector
  {
    public CreateSceneUserControl()
    {
      this.InitializeComponent();
    }

    public Scene GetTScene()
    {
      Scene scene = new Scene();
      if (this.StandardValuesControlInstance.IsStartScene())
      {
        if (VisualNovelHandler.CheckForEmptyStartScene())
          VisualNovelHandler.AddStartScene(this.StandardValuesControlInstance.GetSceneName());
        else if (MessageBox.Show("Novel Already has a Start Scene, do you want to overwrite it?", "Notice", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
          VisualNovelHandler.AddStartScene(this.StandardValuesControlInstance.GetSceneName());
      }
      scene.SceneName = this.StandardValuesControlInstance.GetSceneName();
      scene.TextBox = this.TextBoxUserControlInstance.GetTextBox();
      scene.Background = this.BackgroundUserControlInstance.GetBackground();
      scene.Music = this.MusicUserControlInstance.GetMusic();
      scene.Button = this.ButtonCollectionUserControlInstance.GetTButtonList();
      scene.Sprite = this.SpriteCollectionUserControlInstance.GetTSpriteList();
      scene.Status = SceneStatus.Finished;
      return scene;
    }

    internal Scene GetPreviewTScene()
    {
      return new Scene()
      {
        TextBox = this.TextBoxUserControlInstance.GetPreviewTextBox(),
        Background = this.BackgroundUserControlInstance.GetPreviewBackground(),
        Music = this.MusicUserControlInstance.GetMusic(),
        Button = this.ButtonCollectionUserControlInstance.GetTButtonList(),
        Sprite = this.SpriteCollectionUserControlInstance.GetPreviewTSpriteList()
      };
    }

    internal void ResetScene()
    {
      this.StandardValuesControlInstance.ResetScene();
      this.ButtonCollectionUserControlInstance.ResetScene();
    }

    internal void LoadScene(Scene tempScene)
    {
      this.StandardValuesControlInstance.LoadScene(tempScene);
      this.TextBoxUserControlInstance.LoadScene(tempScene);
      this.BackgroundUserControlInstance.LoadScene(tempScene);
      this.MusicUserControlInstance.LoadScene(tempScene);
      this.SpriteCollectionUserControlInstance.LoadScene(tempScene);
      this.ButtonCollectionUserControlInstance.LoadScene(tempScene);
    }

    public void ApplyVisualNovelTemplate(Scene tempVisualNovelTemplate)
    {
      this.BackgroundUserControlInstance.ApplyVisualNovelTemplate(tempVisualNovelTemplate);
      this.TextBoxUserControlInstance.ApplyVisualNovelTemplate(tempVisualNovelTemplate);
    }

    internal void SetSceneName(string nextSceneName)
    {
      this.StandardValuesControlInstance.SetSceneName(nextSceneName);
    }

    internal void SetSprites(List<Sprite> tempTSpriteList)
    {
      this.SpriteCollectionUserControlInstance.SetSprites(tempTSpriteList);
    }

    internal void StepTextBox(int steps)
    {
      this.TextBoxUserControlInstance.StepTextBox(steps);
    }

    internal void PlaySound()
    {
      this.MusicUserControlInstance.PlaySound();
    }

    internal void StopSound()
    {
      this.MusicUserControlInstance.StopSound();
    }

    internal void AddEmptyButton(string nextSceneName)
    {
      this.ButtonCollectionUserControlInstance.AddEmptyButton(nextSceneName);
    }

    internal void CreateRessources()
    {
      if (this.TextBoxUserControlInstance.GetCustomFileResource() != null)
        VisualNovelHandler.CreateResource(this.TextBoxUserControlInstance.GetCustomFileResource());
      if (this.BackgroundUserControlInstance.GetCustomFileResource() != null)
        VisualNovelHandler.CreateResource(this.BackgroundUserControlInstance.GetCustomFileResource());
      if (this.MusicUserControlInstance.GetCustomFileResource() != null)
        VisualNovelHandler.CreateResource(this.MusicUserControlInstance.GetCustomFileResource());
      foreach (SpriteUserControl spriteUserControl in this.SpriteCollectionUserControlInstance.GetSpriteUserControlList())
      {
        if (spriteUserControl.GetCustomFileResource() != null)
          VisualNovelHandler.CreateResource(spriteUserControl.GetCustomFileResource());
      }
    }

    
  }
}
