﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.OptionsUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class OptionsUserControl : UserControl, IComponentConnector
  {
    public static event OptionsUserControl.showBordersUpdatedDelegate showBordersUpdated;

    public static event OptionsUserControl.setWideScreenDelegate setWideScreen;

    public static event OptionsUserControl.setMaxScreenDelegate setMaxScreen;

    public static event OptionsUserControl.playMusicDelegate playMusic;

    public static event OptionsUserControl.stopMusicDelegate stopMusic;

    public OptionsUserControl()
    {
      this.InitializeComponent();
    }

    private void ShowBordersCheckBox_Checked(object sender, RoutedEventArgs e)
    {
      OptionsUserControl.showBordersUpdated(true);
    }

    private void ShowBordersCheckBox_Unchecked(object sender, RoutedEventArgs e)
    {
      OptionsUserControl.showBordersUpdated(false);
    }

    private void WideScreen_Checked(object sender, RoutedEventArgs e)
    {
      OptionsUserControl.setWideScreen();
    }

    private void MaxScreen_Checked(object sender, RoutedEventArgs e)
    {
      OptionsUserControl.setMaxScreen();
    }

    private void PlayMusicCheckBox_Checked(object sender, RoutedEventArgs e)
    {
      OptionsUserControl.playMusic();
    }

    private void PlayMusicCheckBox_Unchecked(object sender, RoutedEventArgs e)
    {
      OptionsUserControl.stopMusic();
    }

    

    public delegate void showBordersUpdatedDelegate(bool showBorders);

    public delegate void setWideScreenDelegate();

    public delegate void setMaxScreenDelegate();

    public delegate void playMusicDelegate();

    public delegate void stopMusicDelegate();
  }
}
