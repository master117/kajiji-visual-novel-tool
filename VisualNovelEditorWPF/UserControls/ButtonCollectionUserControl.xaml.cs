﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.UserControls.ButtonCollectionUserControl
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace VisualNovelEditorWPF.UserControls
{
  public partial class ButtonCollectionUserControl : UserControl, IComponentConnector
  {
    private readonly List<ButtonUserControl> mainButtonUserControlList = new List<ButtonUserControl>();

    public ButtonCollectionUserControl()
    {
      this.InitializeComponent();
      ButtonUserControl.deleteButton += new ButtonUserControl.deleteButtonDelegate(this.ButtonUserControl_deleteButton);
    }

    private void ButtonUserControl_deleteButton(ButtonUserControl invokingButtonUserControl)
    {
      this.mainButtonUserControlList.Remove(invokingButtonUserControl);
      this.ButtonCollectionButtonStackPanel.Children.Remove((UIElement) invokingButtonUserControl);
    }

    public List<Button> GetTButtonList()
    {
      List<Button> tButtonList = new List<Button>();
      foreach (ButtonUserControl buttonUserControl in this.mainButtonUserControlList)
      {
        if (buttonUserControl.GetButton() != null)
          tButtonList.Add(buttonUserControl.GetButton());
      }
      return tButtonList;
    }

    private void ButtonCollectionAddButton_Click(object sender, RoutedEventArgs e)
    {
      ButtonUserControl buttonUserControl = new ButtonUserControl();
      this.mainButtonUserControlList.Add(buttonUserControl);
      this.ButtonCollectionButtonStackPanel.Children.Add((UIElement) buttonUserControl);
    }

    internal void AddEmptyButton(string nextSceneName)
    {
      ButtonUserControl buttonUserControl = new ButtonUserControl();
      buttonUserControl.SetNextSceneName(nextSceneName);
      this.mainButtonUserControlList.Add(buttonUserControl);
      this.ButtonCollectionButtonStackPanel.Children.Add((UIElement) buttonUserControl);
    }

    internal void ResetScene()
    {
      this.mainButtonUserControlList.Clear();
      this.ButtonCollectionButtonStackPanel.Children.Clear();
    }

    internal void LoadScene(Scene tempScene)
    {
      foreach (Button tempTButton in tempScene.Button)
      {
        ButtonUserControl buttonUserControl = new ButtonUserControl();
        buttonUserControl.LoadButton(tempTButton);
        this.mainButtonUserControlList.Add(buttonUserControl);
        this.ButtonCollectionButtonStackPanel.Children.Add((UIElement) buttonUserControl);
      }
    }

    
  }
}
