﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.Resources.InterfaceStrings_en
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace VisualNovelEditorWPF.Resources
{
  [CompilerGenerated]
  [DebuggerNonUserCode]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  internal class InterfaceStrings_en
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) InterfaceStrings_en.resourceMan, (object) null))
          InterfaceStrings_en.resourceMan = new ResourceManager("VisualNovelEditorWPF.Resources.InterfaceStrings_en", typeof (InterfaceStrings_en).Assembly);
        return InterfaceStrings_en.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return InterfaceStrings_en.resourceCulture;
      }
      set
      {
        InterfaceStrings_en.resourceCulture = value;
      }
    }

    internal static string Browse
    {
      get
      {
        return InterfaceStrings_en.ResourceManager.GetString("Browse", InterfaceStrings_en.resourceCulture);
      }
    }

    internal InterfaceStrings_en()
    {
    }
  }
}
