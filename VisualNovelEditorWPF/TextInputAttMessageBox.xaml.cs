﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.TextInputAttMessageBox
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;
using VisualNovelEditorWPF.Static;
using VisualNovelEditorWPF.UserControls;

namespace VisualNovelEditorWPF
{
  public partial class TextInputAttMessageBox : Window, IComponentConnector
  {
    private bool aborted;


    public string ResponseText
    {
      get
      {
        return this.ResponseTextBox.Text;
      }
      set
      {
        this.ResponseTextBox.Text = value;
      }
    }

    public List<ButtonAttribute> ResponseTAttributes
    {
      get
      {
        return this.ButtonAttributeCollectionUserControlInstance.GetAttributes();
      }
    }

    public TextInputAttMessageBox()
    {
      this.InitializeComponent();
      this.aborted = true;
      this.ResponseTextBox.Focus();
      if (VisualNovelHandler.GetNovelType() != VisualNovelVisualNovelType.Sim)
        return;
      this.ButtonAttributeCollectionUserControlInstance.Visibility = Visibility.Visible;
    }

    public TextInputAttMessageBox(string headerText)
    {
      this.InitializeComponent();
      this.aborted = true;
      this.HeaderTextBlock.Text = headerText;
      this.ResponseTextBox.Focus();
      if (VisualNovelHandler.GetNovelType() != VisualNovelVisualNovelType.Sim)
        return;
      this.ButtonAttributeCollectionUserControlInstance.Visibility = Visibility.Visible;
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      this.aborted = false;
      this.Close();
    }

    private void ResponseTextBox_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.Key != Key.Return)
        return;
      this.aborted = false;
      this.Close();
    }

    public new bool ShowDialog()
    {
      base.ShowDialog();
      return !this.aborted;
    }
  }
}
