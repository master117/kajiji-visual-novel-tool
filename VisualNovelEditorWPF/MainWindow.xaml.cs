﻿// Decompiled with JetBrains decompiler
// Type: VisualNovelEditorWPF.MainWindow
// Assembly: VisualNovelEditorWPF, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 069B012F-C666-458A-8B88-A51E507996AE
// Assembly location: C:\Users\johan\Documents\Visual Studio 2017\Projects\Kajiji - Visual Novel Tool\VisualNovelEditorWPF\bin\Debug\VisualNovelEditorWPF.exe

using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using VisualNovelEditorWPF.CustomDataTypes;
using VisualNovelEditorWPF.Static;
using VisualNovelEditorWPF.UserControls;

namespace VisualNovelEditorWPF
{
    public partial class MainWindow : Window, IComponentConnector
    {
        public MainWindow()
        {
            StartWindowUserControl.newNovelButton +=
                new StartWindowUserControl.newNovelButtonDelegate(this.StartWindowUserControl_newNovelButton);
            StartWindowUserControl.loadNovelButton +=
                new StartWindowUserControl.loadNovelButtonDelegate(this.StartWindowUserControl_loadNovelButton);
            CreateNovelUserControl.backButton +=
                new CreateNovelUserControl.backButtonDelegate(this.CreateNovelUserControl_backButton);
            CreateNovelUserControl.createNovel +=
                new CreateNovelUserControl.createNovelDelegate(this.CreateNovelUserControl_createNovel);
            AttributeCollectionUserControl.saveAttributesButton +=
                new AttributeCollectionUserControl.saveAttributesButtonDelegate(this
                    .AttributeCollectionUserControl_saveAttributesButton);
            TemplateUserControl.loadTemplate +=
                new TemplateUserControl.templateClickedDelegate(this.TemplateUserControl_loadTemplate);
            TextBoxUserControl.textBoxButtonClicked +=
                new TextBoxUserControl.textBoxButtonClickedDelegate(this.StepTextBoxes);
            PositionUserControl.refreshPosition +=
                new PositionUserControl.refreshPositionDelegate(this.UserControl_Update);
            TextBoxUserControl.textBoxUpdate += new TextBoxUserControl.textBoxUpdateDelegate(this.UserControl_Update);
            SpriteUserControl.spriteUpdate += new SpriteUserControl.spriteUpdateDelegate(this.UserControl_Update);
            BackgroundUserControl.backgroundUpdate +=
                new BackgroundUserControl.backgroundUpdateDelegate(this.UserControl_Update);
            OptionsUserControl.showBordersUpdated +=
                new OptionsUserControl.showBordersUpdatedDelegate(this.OptionsUserControl_showBordersUpdated);
            OptionsUserControl.setWideScreen +=
                new OptionsUserControl.setWideScreenDelegate(this.OptionsUserControl_setWideScreen);
            OptionsUserControl.setMaxScreen +=
                new OptionsUserControl.setMaxScreenDelegate(this.OptionsUserControl_setMaxScreen);
            OptionsUserControl.playMusic += new OptionsUserControl.playMusicDelegate(this.OptionsUserControl_playMusic);
            OptionsUserControl.stopMusic += new OptionsUserControl.stopMusicDelegate(this.OptionsUserControl_stopMusic);
            PreviewSideUserControl.spriteUpdated +=
                new PreviewSideUserControl.spriteUpdatedDelegate(this.PreviewSideUserControl_spriteUpdated);
            PreviewSideUserControl.textBoxClicked +=
                new PreviewSideUserControl.textBoxClickedDelegate(this.StepTextBoxes);
            GraphUserControl.loadScene += new GraphUserControl.loadSceneDelegate(this.GraphUserControl_loadScene);
            GraphUserControl.addButton += new GraphUserControl.addButtonDelegate(this.GraphUserControl_addButton);

            this.InitializeComponent();
            this.WindowState = WindowState.Maximized;
            this.ShowStartWindowGrid();
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            this.UpdateLayout();
            this.CreateSceneGrid.UpdateLayout();
        }

        public void SaveVisualNovel()
        {
            VisualNovelHandler.SaveVisualNovel();
        }

        public void CreateAndAddScene()
        {
            Scene tscene = this.CreateSceneUserControlInstance.GetTScene();
            if (string.IsNullOrEmpty(tscene.SceneName))
            {
                int num1 = (int) MessageBox.Show("Please add a Scenename");
            }
            else
            {
                Scene scene = VisualNovelHandler.GetScene(tscene.SceneName);
                if (scene != null)
                {
                    if (scene.Status != SceneStatus.Temporary && MessageBox.Show("Scene already Exists, Overwrite?",
                            "Problem", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;
                    VisualNovelHandler.DeleteScene(tscene.SceneName);
                }
                foreach (Button tButton in tscene.Button)
                {
                    if (VisualNovelHandler.GetScene(tButton.NextSceneName) == null)
                        VisualNovelHandler.AddScene(new Scene()
                        {
                            SceneName = tButton.NextSceneName,
                            Status = SceneStatus.Temporary
                        });
                }
                this.CreateSceneUserControlInstance.CreateRessources();
                VisualNovelHandler.AddScene(tscene);
                if (VisualNovelHandler.CheckForEmptyStartScene())
                {
                    int num2 = (int) MessageBox.Show(
                        "Scene Successfully created and added to your Novel,\nPlease keep in mind, your Novel has no StartScene yet. \nDon't forget to save your Novel!");
                }
                else
                {
                    int num3 = (int) MessageBox.Show(
                        "Scene Successfully created and added to your Novel. \nDon't forget to save your Novel!");
                }
            }
        }

        public void CreateAndAddSceneAndFollowup()
        {
            Scene tscene = this.CreateSceneUserControlInstance.GetTScene();
            if (tscene.Button.Count > 1)
            {
                int num = (int) MessageBox.Show(
                    "Scene has more than one Button, no automation Follow up Scene creatable");
            }
            else
            {
                string nextSceneName;
                if (tscene.Button.Count == 1)
                {
                    nextSceneName = tscene.Button[0].NextSceneName;
                }
                else
                {
                    nextSceneName = tscene.SceneName + "0";
                    this.CreateSceneUserControlInstance.AddEmptyButton(nextSceneName);
                }
                this.CreateAndAddScene();
                this.CreateSceneUserControlInstance.ResetScene();
                this.CreateSceneUserControlInstance.SetSceneName(nextSceneName);
            }
        }

        private void CreateTemplate()
        {
            TextInputMessageBox textInputMessageBox = new TextInputMessageBox();
            textInputMessageBox.HeaderTextBlock.Text = "Name of the new Template:";
            textInputMessageBox.ShowDialog();
            if (textInputMessageBox.ResponseText == null)
                return;
            VisualNovelTemplateHandler.TemplateCreateDirectorys(textInputMessageBox.ResponseText);
            if (VisualNovelTemplateHandler.CheckForExistingTemplate(textInputMessageBox.ResponseText))
            {
                if (MessageBox.Show("A Template with the given name already exists, overwrite?", "Problem",
                        MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;
                VisualNovelTemplateHandler.DeleteTemplate(textInputMessageBox.ResponseText);
                VisualNovelTemplateHandler.TemplateCreateDirectorys(textInputMessageBox.ResponseText);
            }
            Scene tempTemplate = new Scene();
            tempTemplate.SceneName = textInputMessageBox.ResponseText;
            if (this.CreateSceneUserControlInstance.BackgroundUserControlInstance.GetCustomFileResource() != null)
            {
                VisualNovelTemplateHandler.CreateResourceFromCustomFileResource(
                    this.CreateSceneUserControlInstance.BackgroundUserControlInstance.GetCustomFileResource(),
                    textInputMessageBox.ResponseText);
                tempTemplate.Background.BackgroundImagePath = this.CreateSceneUserControlInstance
                    .BackgroundUserControlInstance.GetCustomFileResource().FileName;
            }
            if (this.CreateSceneUserControlInstance.TextBoxUserControlInstance.GetCustomFileResource() != null)
            {
                VisualNovelTemplateHandler.CreateResourceFromCustomFileResource(
                    this.CreateSceneUserControlInstance.TextBoxUserControlInstance.GetCustomFileResource(),
                    textInputMessageBox.ResponseText);
                tempTemplate.TextBox.TextBoxImagePath = this.CreateSceneUserControlInstance.TextBoxUserControlInstance
                    .GetCustomFileResource().FileName;
            }
            if (this.CreateSceneUserControlInstance.TextBoxUserControlInstance.GetTPosition() != null)
                tempTemplate.TextBox.TextBoxPosition = this.CreateSceneUserControlInstance.TextBoxUserControlInstance
                    .GetTPosition();
            VisualNovelTemplateHandler.CreateNewTemplate(tempTemplate);
        }

        private void ShowStartWindowGrid()
        {
            this.CollapseAllGrids();
            this.StartWindowGrid.Visibility = Visibility.Visible;
        }

        private void ShowCreateNovelGrid()
        {
            this.CollapseAllGrids();
            this.CreateNovelGrid.Visibility = Visibility.Visible;
        }

        private void ShowCreateAttributesGrid()
        {
            this.CollapseAllGrids();
            this.CreateAttributesGrid.Visibility = Visibility.Visible;
        }

        private void ShowCreateSceneGrid()
        {
            this.CollapseAllGrids();
            this.PreviewSideUserControlInstance.Height = this.ActualHeight - 120.0;
            this.GraphUserControlGrid.Height = this.ActualHeight - 120.0;
            this.SceneListBoxUserControlInstance.Height = this.ActualHeight - 120.0;
            this.CreateSceneGridTemplateLabel.Height = this.ActualHeight - 120.0;
            this.CreateSceneGridContentLabel.Height = this.ActualHeight - 120.0;
            this.CreateSceneUserControlInstance.TextBoxUserControlInstance.FontComboBox.SelectedValue =
                (object) new FontFamily("Century Gothic");
            this.TemplateListUserControlInstance.UpdateTemplates();
            this.CreateSceneGrid.Visibility = Visibility.Visible;
        }

        private void CollapseAllGrids()
        {
            this.StartWindowGrid.Visibility = Visibility.Collapsed;
            this.CreateNovelGrid.Visibility = Visibility.Collapsed;
            this.CreateSceneGrid.Visibility = Visibility.Collapsed;
            this.CreateAttributesGrid.Visibility = Visibility.Collapsed;
        }

        private void AddButton(Scene sourceScene, Scene targetScene)
        {
            TextInputAttMessageBox inputAttMessageBox = new TextInputAttMessageBox("Text of the Decision");
            if (!inputAttMessageBox.ShowDialog())
                return;
            VisualNovelHandler.AddButton(new Button()
            {
                NextSceneName = targetScene.SceneName,
                ButtonText = inputAttMessageBox.ResponseText,
                ButtonAttribute = inputAttMessageBox.ResponseTAttributes
            }, sourceScene);
        }

        private void LoadScene(Scene tempScene)
        {
            if (MessageBox.Show("Current Scene will be lost if not Added yet, Continue?", "Attention",
                    MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                return;
            this.CreateSceneUserControlInstance.LoadScene(tempScene);
            this.CreateScenePreviewTabControl.SelectedIndex = 0;
            this.UpdatePreview();
        }

        private void LoadTemplate(Scene tempVisualNovelTemplate)
        {
            this.CreateSceneUserControlInstance.ApplyVisualNovelTemplate(tempVisualNovelTemplate);
        }

        private void SaveNovelButton_Click(object sender, RoutedEventArgs e)
        {
            this.SaveVisualNovel();
        }

        private void CreateSceneGridAddSceneButton_Click(object sender, RoutedEventArgs e)
        {
            this.CreateAndAddScene();
        }

        private void CreateSceneGridAddSceneAndFollowUpButton_Click(object sender, RoutedEventArgs e)
        {
            this.CreateAndAddSceneAndFollowup();
        }

        private void CreateSceneGridCreateTemplateButton_Click(object sender, RoutedEventArgs e)
        {
            this.CreateTemplate();
        }

        private void TabItemTemplates_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            this.TemplateListUserControlInstance.UpdateTemplates();
        }

        private void TabItemGraph_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            this.GraphUserControlInstance.CreateGraph();
            this.GraphUserControlInstance.AddGraph();
        }

        private void TabItemListBox_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            this.SceneListBoxUserControlInstance.UpdateContent();
        }

        public void UpdatePreview()
        {
            if (this.CreateSceneUserControlInstance == null)
                return;
            this.PreviewSideUserControlInstance.SetScene(this.CreateSceneUserControlInstance.GetPreviewTScene());
            this.PreviewSideUserControlInstance.Update();
        }

        private void StartWindowUserControl_newNovelButton()
        {
            this.ShowCreateNovelGrid();
        }

        private void StartWindowUserControl_loadNovelButton()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            try
            {
                openFileDialog.Filter = "VisualNovel (.vn)|*.vn";
                openFileDialog.ShowDialog();
                if (string.IsNullOrEmpty(openFileDialog.FileName))
                    return;
                VisualNovelHandler.SetVisualNovelFromPath(openFileDialog.FileName);
                this.ShowCreateSceneGrid();
            }
            catch (Exception ex)
            {
                int num = (int) MessageBox.Show("Error retrieving Novel \n please check the Novel File for errors");
            }
        }

        private void CreateNovelUserControl_backButton()
        {
            this.ShowStartWindowGrid();
        }

        private void CreateNovelUserControl_createNovel(string tempNovelname,
            VisualNovelVisualNovelType tempVisualNovelType)
        {
            if (VisualNovelHandler.CheckForExistingNovel(tempNovelname) &&
                MessageBox.Show("Warning! Novel already Exists, Overwrite?", "Problem", MessageBoxButton.YesNo,
                    MessageBoxImage.Exclamation) == MessageBoxResult.No)
                return;
            VisualNovelHandler.CreateVisualNovel(tempNovelname, tempVisualNovelType);
            switch (tempVisualNovelType)
            {
                case VisualNovelVisualNovelType.Standard:
                    this.ShowCreateSceneGrid();
                    break;
                case VisualNovelVisualNovelType.Sim:
                    this.ShowCreateAttributesGrid();
                    break;
            }
        }

        private void AttributeCollectionUserControl_saveAttributesButton()
        {
            foreach (Attribute attribute in this.AttributeCollectionUserControlInstance.GetAttributes())
                VisualNovelHandler.AddAttribute(attribute);
            this.ShowCreateSceneGrid();
        }

        private void TemplateUserControl_loadTemplate(Scene tempVisualNovelTemplate)
        {
            this.LoadTemplate(tempVisualNovelTemplate);
        }

        private void UserControl_Update()
        {
            this.UpdatePreview();
        }

        private void OptionsUserControl_showBordersUpdated(bool showBorders)
        {
            this.PreviewSideUserControlInstance.ShowBorders(showBorders);
        }

        private void OptionsUserControl_setWideScreen()
        {
        }

        private void OptionsUserControl_setMaxScreen()
        {
        }

        private void OptionsUserControl_stopMusic()
        {
            this.CreateSceneUserControlInstance.StopSound();
        }

        private void OptionsUserControl_playMusic()
        {
            this.CreateSceneUserControlInstance.PlaySound();
        }

        private void PreviewSideUserControl_spriteUpdated(List<Sprite> tempTSpriteList)
        {
            this.CreateSceneUserControlInstance.SetSprites(tempTSpriteList);
            this.UpdatePreview();
        }

        private void GraphUserControl_loadScene(Scene tempScene)
        {
            this.LoadScene(tempScene);
        }

        private void GraphUserControl_addButton(Scene sourceScene, Scene targetScene)
        {
            this.AddButton(sourceScene, targetScene);
        }

        private void StepTextBoxes(int steps)
        {
            this.UpdatePreview();
            this.CreateSceneUserControlInstance.StepTextBox(steps);
            this.PreviewSideUserControlInstance.StepTextBox(steps);
        }
    }
}
