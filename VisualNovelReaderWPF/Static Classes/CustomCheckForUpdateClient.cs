using System;
using System.Net.Sockets;
using System.Text;
using System.Windows;

namespace VisualNovelReaderWPF.Static
{
    public static class CustomCheckForUpdateClient
    {
        public static bool CheckForUpdate(string versionNumber, string address, int port)
        {
            try
            {
                Socket tempSender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                byte[] tempByteArray = Encoding.ASCII.GetBytes(versionNumber);

                tempSender.Connect(address, port);
                //tempSender.Connect(address, port);

                tempSender.Send(tempByteArray);


                //Creating a Byte Array to store our response
                byte[] bytes = new byte[1024];

                //Storing our response in the byte array while storing the responselength in an int
                int bytesLength = tempSender.Receive(bytes);

                string data = Encoding.ASCII.GetString(bytes, 0, bytesLength);

                if (data == "OK")
                {
                    return true;
                }

                return false;
            }
            catch (SocketException e)
            {
                Console.WriteLine(e.ToString());
                MessageBox.Show("Could not connect to Update Server");
                return false;
            }
        }
    }
}
