using System;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;

namespace VisualNovelReaderWPF.Static
{
    public static class BitmapHandler
    {
        public static BitmapImage getBitmapImage(string tempPath)
        {
            BitmapImage tempTextBoxBitmapImage = new BitmapImage();
            tempTextBoxBitmapImage.BeginInit();

            try
            {
                FileStream tempFileStream = new FileStream(tempPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                MemoryStream tempMemoryStream = new MemoryStream(ReadStreamFully(tempFileStream));
                tempFileStream.Close();
                tempTextBoxBitmapImage.StreamSource = tempMemoryStream;
            }
            catch (Exception)
            {
                MessageBox.Show("Error retrieving a Resource, please recheck your Directorys.");
                return null;
            }

            tempTextBoxBitmapImage.EndInit();
            return tempTextBoxBitmapImage;
        }


        public static byte[] ReadStreamFully(Stream tempFileStreamInput)
        {
            //We Create a MemoryStream which we can form into an Byte Array
            using (MemoryStream tempMemoryStream = new MemoryStream())
            {
                tempFileStreamInput.CopyTo(tempMemoryStream);
                return tempMemoryStream.ToArray();
            }
        }
    }
}
