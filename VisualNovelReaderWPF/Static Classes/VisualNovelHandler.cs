namespace VisualNovelReaderWPF.Static
{
    public static class VisualNovelHandler
    {
        private static VisualNovel mainVisualNovel { get; set; }
        private static string mainVisualNovelPath;

        public static void LoadNovel(string tempPath)
        {
            mainVisualNovel = VisualNovel.LoadFromFile(tempPath);

            //We transform our temPath in the Directory our Novel is lying
            //we splitt our oath in arrays between each "/" 
            //and then remove the last arrays length from the path
            string[] tempStringList = tempPath.Split('/');
            mainVisualNovelPath = tempPath.Substring(0, tempPath.Length - tempStringList[tempStringList.Length - 1].Length);
        }

        internal static bool ContainsTScene(string tempSceneName)
        {
            var tempScene = GetTScene(tempSceneName);
            if (tempScene != null && tempScene.Status != tSceneStatus.Temporary)
            {
                return true;
            }

            return false;
        }

        public static tScene GetTScene(string tempSceneName)
        {
            if (mainVisualNovel != null)
            {
                foreach (tScene tempTScene in mainVisualNovel.Scene)
                {
                    if (tempTScene.SceneName == tempSceneName)
                    {
                        return tempTScene;
                   }
               }
            }
            //TODO implement scene not found error message
            return null;
        }

        //This Scene delivers the startscene of teh Novel
        internal static tScene GetStartTScene()
        {
            //This should always work
            if (mainVisualNovel != null)
            {
                //We iterate trough the Scenes and look which ones Name matches our VisualNovel.StartScene
                foreach (tScene tempTScene in mainVisualNovel.Scene)
                {
                    if (tempTScene.SceneName == mainVisualNovel.StartScene)
                    {
                        return tempTScene;
                    }
                }
            }

            //TODO error message
            return null;
        }

        public static string GetResourceDirectory()
        {
            return mainVisualNovelPath + mainVisualNovel.VisualNovelName + Properties.Settings.Default.ResourcesDirectory;
        }
    }
}
