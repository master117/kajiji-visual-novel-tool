namespace VisualNovelReaderWPF.Static
{
    class TextBoxContentCalculator
    {
        /*
        //This method gets content and the Textbox and creates the content accordingly in a string array
        public static string[] CalculateContent(string tempContent, string tempSpeakername, TextBlock tempTextbox)
        {
            
            //We choose the seperating characters and split our text according to them
            char[] splittingChars = { ' ', '\n' };
            string[] splittedWords = tempContent.Split(splittingChars);

            string tempLineString = "";

            FormattedText currentWordFormatted = new FormattedText("A", CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight,
                                                        new Typeface(tempTextbox.FontFamily.ToString()), tempTextbox.FontSize, Brushes.Black);
            //We calculate the height of our font
            double fontHeight = currentWordFormatted.Height;

            //We create a list where we add our finished Textblocks too
            List<string> responseList = new List<string>();

            //If we get an empty input (namely because there is no Text yet, we return a single empty string arrayblock
            if (splittedWords.Length == 1 && splittedWords[0] == "")
            {
                responseList.Add("");
                return responseList.ToArray();
            }

            //We add a counter to count the number of progressed words
            int wordsCounter = 0;

            //if our viewport is not existent, abort
            if (tempTextbox.ViewportHeight == 0.0)
            {
                return null;
            }

            //While we still have words 
            while (wordsCounter < splittedWords.Length)
            {
                //We continue while our Box height isn't filled
                for (double currentBoxHeight = fontHeight; currentBoxHeight <= tempTextbox.ViewportHeight && wordsCounter < splittedWords.Length; currentBoxHeight += fontHeight)
                {
                    for (double currentBoxWidth = 0.0; wordsCounter < splittedWords.Length; wordsCounter++)
                    {
                        //We calculate our current words width + the width of a period (instad of a blank which isn't calculated correct)
                        currentWordFormatted = new FormattedText(splittedWords[wordsCounter] + ".", CultureInfo.GetCultureInfo("en-us"),
                                                      FlowDirection.LeftToRight, new Typeface(tempTextbox.FontFamily.ToString()),
                                                      tempTextbox.FontSize, Brushes.Black);

                        if (currentBoxWidth + currentWordFormatted.Width < tempTextbox.ViewportWidth)
                        {
                            //If our current word is still matching in we add it + a blank
                            tempLineString += splittedWords[wordsCounter] + " ";
                        }
                        else
                        {
                            break;
                        }

                        //We add our used width to the words width
                        currentBoxWidth += currentWordFormatted.Width;
                    }
                    //Adding a blank because we reached our line End
                    tempLineString += "\n";
                }
                //Our Box is full, so we flush our text and create a new Box
                responseList.Add(tempLineString);
                tempLineString = "";
            }


            //We turn our List in an array and return it
            return responseList.ToArray();

        }
         */







        /*
        public static string[] CalculateContent2(string content, string speakername, TextBox textbox)
        {
            char[] delim = { ' ', '\n' };
            string[] words = content.Split(delim);

            string tmpString = "";
            FormattedText formatted = new FormattedText("A",
                CultureInfo.GetCultureInfo("en-us"),
                FlowDirection.LeftToRight,
                new Typeface(textbox.FontFamily.ToString()),
                textbox.FontSize,
                Brushes.Black);

            double fontHeight = formatted.Height;

            List<string> res = new List<string>();
            if (words.Length == 1)
            {
                res.Add("");
                return res.ToArray();
            }
            int word = 0;
            while (word < words.Length)
            {
                for (double height = fontHeight; height < textbox.Height && word < words.Length; height += fontHeight)
                {
                    for (double width = .0; word < words.Length; word++)
                    {
                        formatted = new FormattedText(words[word] + ".",
                            CultureInfo.GetCultureInfo("en-us"),
                            FlowDirection.LeftToRight,
                            new Typeface(textbox.FontFamily.ToString()),
                            textbox.FontSize,
                            Brushes.Black);

                        if (width + formatted.Width < textbox.ViewportWidth)
                            tmpString += words[word] + " ";
                        else
                            break;
                        width += formatted.Width;
                    }
                    tmpString += "\n";
                }
                res.Add(tmpString);
                tmpString = "";
            }

            return res.ToArray();
        }
        */
    }
}
