using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;
using NAudio.Wave;
using NAudio.WindowsMediaFormat;

namespace VisualNovelReaderWPF.Static
{
    public static class SoundPlayerHandler
    {
        //Declarations required for audio out and the MP3 stream
        private static readonly IWavePlayer waveOutDevice;
        private static WaveStream mainOutputStream;

        private static readonly DispatcherTimer loopTimer;
        private static String soundFilePath;

        static SoundPlayerHandler()
        {
            waveOutDevice = new WaveOut();
            //waveOutDevice.PlaybackStopped += waveOutDevice_PlaybackStopped;

            loopTimer = new DispatcherTimer();
            loopTimer.Tick += waveOutDevice_PlaybackStopped;
        }

        public static void Initialize()
        {
            //
            //This is hardcoded background music
            //
            Assembly asm = Assembly.GetExecutingAssembly();
            String[] temp = asm.GetManifestResourceNames();
            Stream soundStream = asm.GetManifestResourceStream("VisualNovelReaderWPF.Music.MainMenuBackgroundMusic.wav");
            WaveStream waveReader = new WaveFileReader(soundStream);
            mainOutputStream = new WaveChannel32(waveReader);
            waveOutDevice.Init(mainOutputStream);
            TurnOn();
        }

        public static void TurnOn()
        {
            //this method turns the radio on
            waveOutDevice.Play();

            //If we choose to have our song repeated this timer will call the repeat function
            //We take the timer timespan from the song
            if (true)
            {
                loopTimer.Interval = mainOutputStream.TotalTime;
                loopTimer.Start();
            }
        }

        public static void TurnOff()
        {
            loopTimer.Stop();
            if (CheckStatus())
            {
                waveOutDevice.Stop();
            }
        }

        public static void ChangeSong(string tempPath)
        {
            //This Method changes the song, regardless if the songplayer is turned on or not
            try
            {
                //Checking that we got a path
                if (tempPath != null)
                {
                    //Creating a stream from our path and backuping it
                    mainOutputStream = CreateInputStream(tempPath);
                    soundFilePath = tempPath;
                }

                //If the Soundplayer is turned on, we change the song and switch it on again
                //otherwise we just switch the song               
                if (CheckStatus())
                {
                    waveOutDevice.Init(mainOutputStream);
                    TurnOn();
                }
                else
                {
                    waveOutDevice.Init(mainOutputStream);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid or Missing Soundfile please check: " + tempPath);
            }
        }

        public static bool CheckStatus()
        {
            return waveOutDevice != null && waveOutDevice.PlaybackState != PlaybackState.Stopped;
        }


        public static void Repeat()
        {
            //This method plays the current song again
            if (soundFilePath != null)
            {
                mainOutputStream = CreateInputStream(soundFilePath);
                waveOutDevice.Init(mainOutputStream);
                TurnOn();
            }
        }

        //
        // Events
        //
        private static void waveOutDevice_PlaybackStopped(object sender, EventArgs e)
        {
            loopTimer.Stop();
            Repeat();
        }

        //
        // StreamReader
        //
        private static WaveStream CreateInputStream(string fileName)
        {
            WaveChannel32 inputStream = null;

            if (fileName.EndsWith(".mp3"))
            {
                WaveStream mp3Reader = new Mp3FileReader(fileName);
                inputStream = new WaveChannel32(mp3Reader);                
            }

            if(fileName.EndsWith(".wav"))
            {
                WaveStream waveReader = new WaveFileReader(fileName);
                inputStream = new WaveChannel32(waveReader);
            }

            if (fileName.EndsWith(".wma"))
            {
                //TODO divide by 0 exception
                WaveStream wmaReader = new WMAFileReader(fileName);
                inputStream = new WaveChannel32(wmaReader);
            }

            if (fileName.EndsWith(".aiff"))
            {
                WaveStream aiffReader = new AiffFileReader(fileName);
                inputStream = new WaveChannel32(aiffReader);
            }

            if (inputStream == null)
            {
                throw new InvalidOperationException("Unsupported extension");
            }

            return inputStream;
        }
    }
}
