using System.Windows;
using System.Windows.Controls;

namespace VisualNovelReaderWPF.Static
{
    public static class StaticMethods
    {
        public static tPosition standardSpriteTPosition;
        public static tPosition standardTextBoxTPosition;

        //
        //Static Controls
        //
        public static void UpdateControlPosition(Control tempInnerControl, Grid tempOuterGrid, tPosition tempPosition)
        {
            double leftMargin = (tempOuterGrid.Width / 100) * tempPosition.PositionLeft;
            double topMargin = (tempOuterGrid.Height / 100) * tempPosition.PositionTop;
            double rightMargin = (tempOuterGrid.Width / 100) * tempPosition.PositionRight;
            double bottomMargin = (tempOuterGrid.Height / 100) * tempPosition.PositionBottom;

            double width = tempOuterGrid.Width - leftMargin - rightMargin;
            double height = tempOuterGrid.Height - topMargin - bottomMargin;

            tempInnerControl.Margin = new Thickness(leftMargin, topMargin, rightMargin, bottomMargin);
            tempInnerControl.Width = width;
            tempInnerControl.Height = height;
        }

        public static void UpdateControlPosition(Image tempInnerControl, Grid tempOuterGrid, tPosition tempPosition)
        {
            double leftMargin = (tempOuterGrid.Width / 100) * tempPosition.PositionLeft;
            double topMargin = (tempOuterGrid.Height / 100) * tempPosition.PositionTop;
            double rightMargin = (tempOuterGrid.Width / 100) * tempPosition.PositionRight;
            double bottomMargin = (tempOuterGrid.Height / 100) * tempPosition.PositionBottom;

            double width = tempOuterGrid.Width - leftMargin - rightMargin;
            double height = tempOuterGrid.Height - topMargin - bottomMargin;

            tempInnerControl.Margin = new Thickness(leftMargin, topMargin, rightMargin, bottomMargin);
            tempInnerControl.Width = width;
            tempInnerControl.Height = height;
        }

        //
        // Size changing but already drawn Controls
        //
        public static void UpdateActualControlPosition(TextBlock tempInnerControl, Grid tempOuterGrid, tPosition tempPosition)
        {
            double leftMargin = (tempOuterGrid.ActualWidth / 100) * tempPosition.PositionLeft;
            double topMargin = (tempOuterGrid.ActualHeight / 100) * tempPosition.PositionTop;
            double rightMargin = (tempOuterGrid.ActualWidth / 100) * tempPosition.PositionRight;
            double bottomMargin = (tempOuterGrid.ActualHeight / 100) * tempPosition.PositionBottom;

            double width = tempOuterGrid.ActualWidth - leftMargin - rightMargin;
            double height = tempOuterGrid.ActualHeight - topMargin - bottomMargin;

            tempInnerControl.Margin = new Thickness(leftMargin, topMargin, rightMargin, bottomMargin);
            tempInnerControl.Width = width;
            tempInnerControl.Height = height;
        }

        public static void UpdateActualControlPosition(Image tempInnerControl, Grid tempOuterGrid, tPosition tempPosition)
        {
            double leftMargin = (tempOuterGrid.ActualWidth / 100) * tempPosition.PositionLeft;
            double topMargin = (tempOuterGrid.ActualHeight / 100) * tempPosition.PositionTop;
            double rightMargin = (tempOuterGrid.ActualWidth / 100) * tempPosition.PositionRight;
            double bottomMargin = (tempOuterGrid.ActualHeight / 100) * tempPosition.PositionBottom;

            double width = tempOuterGrid.ActualWidth - leftMargin - rightMargin;
            double height = tempOuterGrid.ActualHeight - topMargin - bottomMargin;

            tempInnerControl.Margin = new Thickness(leftMargin, topMargin, rightMargin, bottomMargin);
            tempInnerControl.Width = width;
            tempInnerControl.Height = height;
        }

        //SUGGEST This may be faster and should be used later on
        //THis works for a fixed size Grid, actualwidth works only if the grid was already drawn, one has to choose which to use
        /*
        tempInnerControl.Margin = new Thickness(tempOuterGrid.Width / 100 * tempPosition.PositionLeft, tempOuterGrid.Height / 100 * tempPosition.PositionTop, 0, 0);
        tempInnerControl.Width = tempOuterGrid.Width - tempOuterGrid.Width / 100 * tempPosition.PositionRight - tempOuterGrid.Width / 100 * tempPosition.PositionLeft;
        tempInnerControl.Height = tempOuterGrid.Height - tempOuterGrid.Height / 100 * tempPosition.PositionBottom - tempOuterGrid.Height / 100 * tempPosition.PositionTop;
         * */
    }
}
