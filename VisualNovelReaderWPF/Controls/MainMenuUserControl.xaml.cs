﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VisualNovelReaderWPF.Controls
{
    /// <summary>
    /// Interaktionslogik für MainMenuUserControl.xaml
    /// </summary>
    public partial class MainMenuUserControl : UserControl
    {
        public delegate void openOptionsDelegate();
        public static event openOptionsDelegate openMainMenuOptions;

        public delegate void openNewGameDelegate();
        public static event openNewGameDelegate openMainMenuNewGame;

        public MainMenuUserControl()
        {
            InitializeComponent();
        }

        private void MenuGridOptionsButton_Click(object sender, RoutedEventArgs e)
        {
            openMainMenuOptions();
        }

        private void MenuGridNewGameButton_Click(object sender, RoutedEventArgs e)
        {
            openMainMenuNewGame();
        }
    }
}
