﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VisualNovelReaderWPF.Controls
{
    /// <summary>
    /// Interaktionslogik für StartWindowUserControl.xaml
    /// </summary>
    public partial class StartWindowUserControl : UserControl
    {
        public delegate void startProgramDelegate();
        public static event startProgramDelegate startProgram;

        public StartWindowUserControl()
        {
            InitializeComponent();
        }

        private void StartGridStartButton_Click(object sender, RoutedEventArgs e)
        {
            startProgram();
        }
    }
}
