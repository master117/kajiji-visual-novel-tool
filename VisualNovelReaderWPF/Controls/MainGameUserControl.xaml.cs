﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using VisualNovelReaderWPF.CustomDataTypes;
using VisualNovelReaderWPF.Static;

namespace VisualNovelReaderWPF.Controls
{
    /// <summary>
    /// Interaktionslogik für MainGameUserControl.xaml
    /// </summary>
    public partial class MainGameUserControl : UserControl
    {
        int textBoxPanelCounter = 0;
        string[] textBoxContent;
        tScene currentTScene;

        public MainGameUserControl()
        {
            InitializeComponent();
        }

        public void LoadTScene(tScene tempTScene)
        {
            currentTScene = tempTScene;

            Update();
        }

        //
        // Updating our current scene
        //

        public void Update()
        {
            //Making sure we run in no threading problem
            VerifyAccess();

            //If this is the first time invoked we need to set our Scene to the StartScene
            if (currentTScene == null)
            {
                currentTScene = VisualNovelHandler.GetStartTScene();
            }

            UpdateBackgroundBackground();

            UpdateSprites();

            UpdateTextBox();

            UpdateTextBoxContent();

            UpdateMusic();

            UpdateButtons();
        }

        private void UpdateBackgroundBackground()
        {
            if (currentTScene.Background.BackgroundImagePath != null)
            {
                BackgroundImage.BeginInit();
                BackgroundImage.Source 
                    = BitmapHandler.getBitmapImage((VisualNovelHandler.GetResourceDirectory() + currentTScene.Background.BackgroundImagePath));
                BackgroundImage.EndInit();
            }
        }

        private void UpdateSprites()
        {
            //TODO improve this in the editor
            //First all sprites are cleared from the spritespacegrid, a place for sprites
            SpriteSpaceGrid.Children.Clear();

            //For each tSprite in our list we do the following
            //if we have no sprites -> no problem
            foreach (tSprite tempTSprite in currentTScene.Sprite)
            {
                //We check if the file resource of the sprite is existing
                if (tempTSprite.SpriteImagePath != null && tempTSprite.SpriteImagePath != "")
                {
                    string[] filePathArray = tempTSprite.SpriteImagePath.Split('/');
                    string[] fileDescriptorCheckArray = filePathArray[filePathArray.Length - 1].Split('.');

                    //We create the path from our variable and the path naming
                    string tempImagePath = VisualNovelHandler.GetResourceDirectory() + tempTSprite.SpriteImagePath;

                    if (fileDescriptorCheckArray[fileDescriptorCheckArray.Length - 1] != "gif")
                    {
                        //Making a new Image
                        Image tempImage = new Image();
                        //Getting the Images Source from our resource
                        tempImage.BeginInit();
                        tempImage.Source = BitmapHandler.getBitmapImage(tempImagePath);
                        tempImage.EndInit();
                        tempImage.Margin = new Thickness(0);
                        tempImage.Stretch = Stretch.Fill;

                        //Checking and Flipping our Image
                        if (tempTSprite.Flipped == tSpriteFlipped.Flipped)
                        {
                            //Here we flip our sprite since well duh, thats what the user wished for
                            //We set the scale origin in the images center, then create a ScaleTransform with
                            //we set the ScaleTransform's scaleX to -1 so it flips
                            tempImage.RenderTransformOrigin = new Point(0.5, 0.5);
                            ScaleTransform tempScaleTransform = new ScaleTransform();
                            tempScaleTransform.ScaleX = -1;
                            tempImage.RenderTransform = tempScaleTransform;
                        }

                        StaticMethods.UpdateActualControlPosition(tempImage, MainGameGrid, tempTSprite.SpritePosition);

                        //Adding our finished Border which contains the Image to our Preview
                        SpriteSpaceGrid.Children.Add(tempImage);
                    }
                    else
                    {
                        //Making a new GifImage
                        GifImage tempGifImage = new GifImage();
                        //Getting the Images Source from our ressource
                        tempGifImage.GifSource = tempImagePath;

                        //Setting necessary attributes
                        tempGifImage.VerticalAlignment = VerticalAlignment.Top;
                        tempGifImage.HorizontalAlignment = HorizontalAlignment.Left;
                        tempGifImage.Stretch = Stretch.Fill;
                        tempGifImage.AutoStart = true;

                        //Checking and Flipping our Image
                        if (tempTSprite.Flipped == tSpriteFlipped.Flipped)
                        {
                            //Here we flip our sprite since well duh, thats what the user wished for
                            //We set the scale origin in the images center, then create a ScaleTransform with
                            //we set the ScaleTransform's scaleX to -1 so it flips
                            tempGifImage.RenderTransformOrigin = new Point(0.5, 0.5);
                            ScaleTransform tempScaleTransform = new ScaleTransform();
                            tempScaleTransform.ScaleX = -1;
                            tempGifImage.RenderTransform = tempScaleTransform;
                        }

                        //Setting the Borders position from the Images position
                        StaticMethods.UpdateActualControlPosition(tempGifImage, MainGameGrid, tempTSprite.SpritePosition);

                        SpriteSpaceGrid.Children.Add(tempGifImage);
                    }
                }
            }
        }

        private void UpdateTextBox()
        {
            //Textbox position is always set, therefore we need no null check
            StaticMethods.UpdateActualControlPosition(TextBoxTextBlock, MainGameGrid, currentTScene.TextBox.TextBoxPosition);

            if (currentTScene.TextBox.TextBoxImagePath != null)
            {
                TextBoxTextBlock.Background 
                    = new ImageBrush(BitmapHandler.getBitmapImage(VisualNovelHandler.GetResourceDirectory() + currentTScene.TextBox.TextBoxImagePath));
            }

            //TODO
            /*
            TextBoxTextBlock.Foreground = new SolidColorBrush(tempColor);
            TextBoxTextBlock.FontFamily = tempFontFamily;
            TextBoxTextBlock.FontSize = tempFontSize;
             */
        }

        public void UpdateTextBoxContent()
        {
            textBoxContent = CreateTextBoxContent(currentTScene.TextBox.Text, "You");
            textBoxPanelCounter = 0;
            StepTextBoxContent();
        }

        public string[] CreateTextBoxContent(List<string> tempTextList, string tempSpeakerName)
        {
            return tempTextList.ToArray(); //TextBoxContentCalculator.CalculateContent(tempText, tempSpeakerName, TextBoxTextBlock);
        }

        private void UpdateMusic()
        {
            if (currentTScene.Music.MusicFilePath != null)
            {
                SoundPlayerHandler.ChangeSong(currentTScene.Music.MusicFilePath);
            }
        }

        //Here we add a button for each decision to our buttongrid
        private void UpdateButtons()
        {
            ButtonStackPanel.Children.Clear();

            //TODO

            //For each Button our our scene
            foreach(var tempTSceneButton in currentTScene.Button)
            {
                //we create a new button, which we configure and then add
                Button tempButton = new Button();

                //we need to differentiate the button text if it is a sim novel
                //TODO

                tempButton.Content = tempTSceneButton.ButtonText;
                tempButton.Tag = tempTSceneButton.NextSceneName;
                tempButton.Background = new SolidColorBrush(Colors.LightGoldenrodYellow);
                tempButton.Margin = new Thickness(0, 30, 0, 30);
                tempButton.Click += decisionButton_Click;

                //TODO remove magic numbers
                tempButton.MinWidth = 200;
                tempButton.MinHeight = 30;

                ButtonStackPanel.Children.Add(tempButton);
            }
        }

        //
        // UIEvent Handling
        //

        private void LeftClickRichTextBox(object sender, MouseButtonEventArgs e)
        {
            ForwardTextBoxContent();
        }

        private void RightClickRichTextBox(object sender, MouseButtonEventArgs e)
        {
            BackwardTextBoxContent();
        }

        private void RightClickBackgroundImage(object sender, MouseButtonEventArgs e)
        {
            if (TextBoxTextBlock.Visibility == Visibility.Collapsed && ButtonStackPanel.Visibility == Visibility.Collapsed)
            {
                TextBoxTextBlock.Visibility = Visibility.Visible;
                ButtonStackPanel.Visibility = Visibility.Visible;
            }
            else
            {
                TextBoxTextBlock.Visibility = Visibility.Collapsed;
                ButtonStackPanel.Visibility = Visibility.Collapsed;
            }
        }

        //
        // Event Handling
        //
       
        public void ForwardTextBoxContent()
        {
            if (textBoxContent != null)
            {
                textBoxPanelCounter++;
                if (textBoxContent.Length > textBoxPanelCounter)
                {
                    StepTextBoxContent();
                }
                else
                {
                    textBoxPanelCounter = textBoxContent.Length;
                    ShowButtons();
                }
            }
        }

        public void BackwardTextBoxContent()
        {
            if (textBoxContent != null)
            {
                if (textBoxPanelCounter > 0)
                {
                    textBoxPanelCounter--;
                }
                StepTextBoxContent();
            }
        }

        public void StepTextBoxContent()
        {
            ButtonStackPanel.Visibility = Visibility.Collapsed;
            TextBoxTextBlock.Visibility = Visibility.Visible;

            //TODO find out how there can be an error here :S
            if (textBoxContent != null)
            {
                TextBoxTextBlock.Text = textBoxContent[textBoxPanelCounter];
            }
        }

        public void ShowButtons()
        {
            TextBoxTextBlock.Visibility = Visibility.Collapsed;
            ButtonStackPanel.Visibility = Visibility.Visible;
        }


        void decisionButton_Click(object sender, RoutedEventArgs e)
        {
            Button tempButton = (Button)sender;


            if (VisualNovelHandler.ContainsTScene(tempButton.Tag.ToString()))
            {
                currentTScene = VisualNovelHandler.GetTScene(tempButton.Tag.ToString());
                Update();
                return;
            }

            MessageBox.Show("Error, Scene not found, please check the VN File");
        }
    }
}
