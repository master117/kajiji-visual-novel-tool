﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VisualNovelReaderWPF.Static;

namespace VisualNovelReaderWPF.Controls
{
    /// <summary>
    /// Interaktionslogik für MainMenuOptionsUserControl.xaml
    /// </summary>
    public partial class MainMenuOptionsUserControl : UserControl
    {
        public MainMenuOptionsUserControl()
        {
            InitializeComponent();
        }

        public void Update()
        {
            SetMusicButtonsBorder();
        }

        private void OptionsGrindMusicOnButton_Click(object sender, RoutedEventArgs e)
        {
            SoundPlayerHandler.TurnOn();
            SetMusicButtonsBorder();
        }

        private void OptionsGrindMusicOffButton_Click(object sender, RoutedEventArgs e)
        {
            SoundPlayerHandler.TurnOff();
            SetMusicButtonsBorder();
        }

        private void SetMusicButtonsBorder()
        {
            if (SoundPlayerHandler.CheckStatus())
            {
                OptionsGrindMusicOnButton.BorderThickness = new Thickness(1);
                OptionsGrindMusicOffButton.BorderThickness = new Thickness(0);
                UpdateLayout();
            }
            else
            {
                OptionsGrindMusicOnButton.BorderThickness = new Thickness(0);
                OptionsGrindMusicOffButton.BorderThickness = new Thickness(1);
            }
        }
    }
}
