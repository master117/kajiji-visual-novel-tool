﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VisualNovelReaderWPF.Controls
{
    /// <summary>
    /// Interaktionslogik für NewGameMenuUserControl.xaml
    /// </summary>
    public partial class MainMenuNewGameUserControl : UserControl
    {
        public delegate void openMainGameDelegate(string tempNovelPath);
        public static event openMainGameDelegate openMainGame;

        public MainMenuNewGameUserControl()
        {
            InitializeComponent();
        }

        public void Update()
        {
            if (Directory.Exists(Properties.Settings.Default.VisualNovelDirectory))
            {
                foreach (string tempFileName in Directory.GetFiles(Properties.Settings.Default.VisualNovelDirectory, "*.vn"))
                {
                    Button tempButton = new Button();
                    tempButton.Content = tempFileName;
                    tempButton.Tag = tempFileName;
                    tempButton.FontSize = 20.0;
                    //TODO make font a property
                    tempButton.FontFamily = new FontFamily("Century Gothic");

                    tempButton.Click += NovelButton_Click;

                    NewGameGridNovelSpaceListView.Items.Add(tempButton);
                }
            }
        }

        private void NovelButton_Click(object sender, RoutedEventArgs e)
        {
            Button tempButton = (Button)sender;

            openMainGame((string)tempButton.Tag);
        }

        private void NewGameGridLoadNovelButton_Click(object sender, RoutedEventArgs e)
        {
            var openDialg = new OpenFileDialog();

            try
            {
                openDialg.Filter = "VisualNovel (.vn)|*.vn";
                openDialg.ShowDialog();

                if (openDialg.FileName == "" || openDialg.FileName == null)
                {
                    return;
                }

                openMainGame(openDialg.FileName);
            }
            catch (Exception)
            {

            } 
        }
    }
}
