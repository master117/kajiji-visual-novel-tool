﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using VisualNovelReaderWPF.Static;

namespace VisualNovelReaderWPF
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class SplashScreenWindow : Window
    {
        public delegate void contentLoadedDelegate();
        public static event contentLoadedDelegate contentLoaded;

        public SplashScreenWindow()
        {
            InitializeComponent();
        }

        protected override void OnContentRendered(EventArgs e)
        {
            base.OnContentRendered(e);

            //TODO maybe remove, but time loss is insignificant
            //Creating local Variable tempSettings to make code easier to read
            var tempSett = Properties.Settings.Default;
            
            if (!CustomCheckForUpdateClient.CheckForUpdate(tempSett.VersionNumber, tempSett.UpdateServer, tempSett.UpdateServerPort))
            {
                MessageBox.Show("This Version of the Programm may has expired, \n there is no guarantee for functionality");
            }
            
            Thread.Sleep(2000);

            contentLoaded();

            this.Close();
        }
    }
}
