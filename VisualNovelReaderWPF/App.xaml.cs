﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace VisualNovelReaderWPF
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            SplashScreenWindow.contentLoaded += OpenStartWindow;
        }

        public void OpenStartWindow()
        {
            var tempStartWindow = new MainWindow();
            Current.MainWindow = tempStartWindow;
            tempStartWindow.Show();
        }
    }
}
