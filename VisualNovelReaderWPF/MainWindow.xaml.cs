﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using VisualNovelReaderWPF.Controls;
using VisualNovelReaderWPF.CustomDataTypes;
using VisualNovelReaderWPF.Static;

namespace VisualNovelReaderWPF
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //TODO make private
        //Here we always store the Current Scene we are in
        public tScene currentMainVisualNovelTScene { get; set; }

        DispatcherTimer updateTimer = new DispatcherTimer();

        public MainWindow()
        {
            //StandardInitializer
            InitializeComponent();

            //Setting the Window Title
            this.Title = "Kajiji - Visual Novel Reader - Copyright Johannes Gocke - Master117© 2012";

            //This is our first Window, so we Maximize it until a config storage is Implemented
            //Todo Implement Config Storage
            this.WindowState = WindowState.Maximized;

            //Initialize Soundplayer
            SoundPlayerHandler.Initialize();

            //Adding the Events
            StartWindowUserControl.startProgram += startProgram;
            MainMenuUserControl.openMainMenuOptions += openMainMenuOptions;
            MainMenuUserControl.openMainMenuNewGame += openMainMenuNewGame;
            MainMenuNewGameUserControl.openMainGame += openMainGame;

            updateTimer.Interval = new TimeSpan(100);
            updateTimer.Tick += updateTimer_Tick;

            StartGrid.Visibility = Visibility.Visible;
        }

        #region Event Handling
        private void startProgram()
        {
            CollapseAllGrids();

            MainMenuGrid.Visibility = Visibility.Visible;
        }

        private void openMainMenuOptions()
        {
            CollapseAllGrids();

            MainMenuOptionsUserControlInstance.Update();

            MainMenuOptionsGrid.Visibility = Visibility.Visible;
        }

        private void openMainMenuNewGame()
        {
            CollapseAllGrids();

            MainMenuNewGameUserControlInstance.Update();

            MainMenuNewGameGrid.Visibility = Visibility.Visible;
        }

        private void openMainGame(string tempNovelPath)
        {
            VisualNovelHandler.LoadNovel(tempNovelPath);

            CollapseAllGrids();

            MainGameGrid.Visibility = Visibility.Visible;

            updateTimer.Start();       
        }

        void updateTimer_Tick(object sender, EventArgs e)
        {
            updateTimer.Stop();

            MainGameUserControlInstance.Update();     
        }

        

        private void CollapseAllGrids()
        {
            StartGrid.Visibility = Visibility.Collapsed;
            MainMenuGrid.Visibility = Visibility.Collapsed;
            MainMenuNewGameGrid.Visibility = Visibility.Collapsed;
            MainMenuOptionsGrid.Visibility = Visibility.Collapsed;
            MainGameGrid.Visibility = Visibility.Collapsed;
        }
        #endregion
    }
}
