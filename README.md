# Kajiji - The Visual Novel Tool #

© 2017 JOHANNES GOCKE

![Kajiji_Visual_Novel_Engine_Logo_620x300_Trans.png](https://bitbucket.org/repo/akBzpb/images/249376962-Kajiji_Visual_Novel_Engine_Logo_620x300_Trans.png)

Kajiji is an open source visual novel editor and reader in WPF/WF and RT.

### Features ###
Editor:
![SampleScene.jpg](https://bitbucket.org/repo/akBzpb/images/221658741-SampleScene.jpg)

Template creation and use:
![ConceptTem.png](https://bitbucket.org/repo/akBzpb/images/2119047762-ConceptTem.png)

Drag and drop sprite manipulation, including border view:
![ConceptBor.png](https://bitbucket.org/repo/akBzpb/images/1635277677-ConceptBor.png)

Scene connection visualizations as lists or Shaw-Fujikawa trees:
![ConGra.png](https://bitbucket.org/repo/akBzpb/images/1738640313-ConGra.png)
![ConceptLis.png](https://bitbucket.org/repo/akBzpb/images/804836806-ConceptLis.png)

Sim game like attribute system possible:
![ConceptAtt.png](https://bitbucket.org/repo/akBzpb/images/3816764005-ConceptAtt.png)



### Visual Novel file format ###
![Kajiji Visual Novel Fileformat Explanation.png](https://bitbucket.org/repo/akBzpb/images/755038602-Kajiji%20Visual%20Novel%20Fileformat%20Explanation.png)

### How to Use ###
* Download binarys from the downloads section (coming soon)
* Run Exe

### Warning ###
Some of the codebase is very old, and may contain bugs.

### Contact ###
* johannes_gocke@hotmail.de